#!/usr/bin/env python3

from netdb_client.api32 import APIEndpoint, APISession;
from netdb_client import util;

parser = util.ArgumentParser(description='Get BCD by ip')
parser.add_argument('ip', help='The ip addres')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

ip = args.ip

ta_data = api.execute_ta([
    {"idx": "subnets", "name": "nd.ip_subnet.list", "old": {"cidr_operator": "smallest_cts", "cidr": ip}},
    {"idx": "bcds", "name": "nd.bcd.list", "join": {"subnets": "default"}},
])

_, bcds = ta_data

print(" ".join((bcd['name'] for bcd in bcds)))
