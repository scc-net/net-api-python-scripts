#!/usr/bin/env python3

import csv
import sys

from netdb_client.api32 import macauth, nd, APIEndpoint, APISession;
from netdb_client import util;

parser = util.ArgumentParser(description='create macauth entries')
parser.add_argument('file', help='file with macauth entries: mac address, description - separated by tab')
parser.add_argument('--bcd', required=True, help='BCD for macauth entry')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
client = APISession(endpoint)

bcds = nd.Bcd.list(client, name_list_old=[args.bcd])

if len(bcds) != 1:
  print(f"BCD {args.bcd} not found or ambiguous.")
  sys.exit(1)

bcd = bcds[0]

ta = []
with open(args.file, newline='') as csvfile:
  reader = csv.reader(csvfile, delimiter="\t")

  for row in reader:
  	if len(row) < 2:
  		print(f"Invalid row in file: {' '.join(row)}")
  		continue

  	mac, descr = row
  	ta.append(macauth.Client.create_ta(bcd_name_new=bcd.name, mac_addr_new=mac, description_new=descr))

client.execute_ta(ta)
