#!/usr/bin/env python3

import re

from netdb_client.api32 import APIEndpoint, APISession
from netdb_client.util import ArgumentParser
from netdb_client.api32.dns import Record

parser = ArgumentParser(description='show records under fqdn')
parser.add_argument('fqdn', help='fqdn')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

#rr = Record.list(api_session=api, fqdn_label_regexp_old='^war-')
fqdn_escape=re.escape(args.fqdn) + '\.$'
rr = Record.list(api_session=api, fqdn_regexp_old=fqdn_escape)

for i in rr:
    #print(i.fqdn)
    try:
      print(i.fqdn + '\t' + i.data + '\t' + i.target_bcd_list[0])
    except:
        pass
