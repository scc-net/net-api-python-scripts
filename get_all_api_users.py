#!/usr/bin/env python3
import csv
import json
from argparse import FileType

import tabulate
from netdb_client.api41 import APIEndpoint, APISession
from netdb_client.util import ArgumentParser

parser = ArgumentParser(description='Find all active NETVS API users.')
parser.add_argument('--massmail',  type=FileType('w'), help='File to write data in KIT-CERT massmailer format')
parser.add_argument('--csv', type=FileType('w'), help='File to write data in CSV format')
parser.add_argument('--unique-mail', action='store_true', help='Deduplicate by e-mail. WARNING: The mgr-object passed to the template may be only one of the matched ones.')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)
ta = [
    {"idx": "get_subs", "name": "cntl.mgr.list", 'old': {'is_sub': True, 'kit_status': 1, 'is_svc': False, 'sorting_params_list': ['email']}},
]
result = api.execute_ta(ta)
seen = dict()
users = []
usr = result[0]
for u in usr:
    if u['login_name'] in seen:
        continue
    seen[u['login_name']] = u
    users.append(u)
users = [u for u in users if u['email'] is not None and u['email'] != '']
if args.unique_mail:
    mails = {u['email']: u for u in users}
    users = mails.values()
headers = ['Name', 'Account', 'E-Mail']
user_list = [
    [f'{user["first_name"]} {user["last_name"]}', user['login_name'], user['email']]
    for user in users]
print(f'Active Users:')
print(tabulate.tabulate(user_list, headers=headers))

if args.massmail is not None:
    cont = dict()
    cont['globals'] = {
        'additional_headers': {
            'Reply-To': 'netvs@scc.kit.edu',
            'X-Auto-Response-Suppress': 'All',
            'Precedence': 'Bulk',
            'Auto-Submitted': 'yes'
        }
    }
    cont['individual'] = list()
    for u in users:
        cont['individual'].append({
            'to': u['email'],
            'name': f'{u["first_name"]} {u["last_name"]}',
            'login_name': u['login_name'],
            })
    args.massmail.write(json.dumps(cont, indent=4))
    print(f'Wrote massmail data into {args.massmail.name}')

if args.csv is not None:
    writer = csv.writer(args.csv)
    writer.writerow(headers)
    writer.writerows(user_list)
    print(f'Wrote data into {args.csv.name}')
