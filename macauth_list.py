#!/usr/bin/env python3

import csv
import sys

from netdb_client.api32 import macauth, nd, APIEndpoint, APISession;
from netdb_client import util;

parser = util.ArgumentParser(description='search macauth entry')
parser.add_argument('--bcd', help='BCD for macauth entry')
parser.add_argument('--mac', help='BCD for macauth entry')
args = parser.parse_args()
bcd = mac = None
if(args.bcd):
    bcd = args.bcd
if(args.mac):
    mac = args.mac

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

ta = []
ta_element = macauth.Client.list_ta()
if mac is not None:
    ta_element['old']['mac_addr'] = mac
if bcd is not None:
    ta_element['old']['bcd_name'] = bcd
ta.append(ta_element)

result = api.execute_ta(ta)

for client in result[0]:
    print(f"{client['bcd_name']}  {client['mac_addr']} {client['description']}")
