#!/usr/bin/env python3

from netdb_client.api32 import dns, nd, APIEndpoint, APISession;
from netdb_client import util;

import ipaddress

parser = util.ArgumentParser(description='list ip addresses in subnet')
parser.add_argument('subnet', help='subnet')
args = parser.parse_args()

subnet = args.subnet

for addr in ipaddress.IPv4Network(subnet):
    print('%s' % addr)

