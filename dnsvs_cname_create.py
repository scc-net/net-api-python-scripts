#!/usr/bin/env python3

from netdb_client.api32 import dns, APIEndpoint, APISession;
from netdb_client import util;

parser = util.ArgumentParser(description='new cname')
parser.add_argument('-r', action='store_true', default=False, help='swap fqdn and redata')
parser.add_argument('file', help='file containing host entries: fqdn rdata, separated by spaces')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

if args.file:
    filename = args.file
    lines = [line.strip() for line in open(filename)]

records = []
for line in lines:
    data = line.split()
    if args.r:
        data = reversed(data)
    (fqdn, rdata) = data
    records.append(
        dns.Record.create_ta(
            fqdn_new=fqdn,
            data_new=rdata,
            type_new="CNAME",
        )
    )

api.execute_ta(records)
