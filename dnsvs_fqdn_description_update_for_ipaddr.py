#!/usr/bin/env python3

from netdb_client.api41 import dns, APIEndpoint, APISession
from netdb_client import util

parser = util.ArgumentParser(description='update fqdn descriptions for ip address')
parser.add_argument('--append', '-a', action="store_true", default=False, help='append previous description')
parser.add_argument('file', help='file containing entries: ipaddr fqdn-desc, separated by tabs')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

if args.file:
    filename = args.file
    lines = [line.strip() for line in open(filename)]

fqdns = []
for line in lines:
    if line.strip():
        [ipaddr, fqdn_desc] = line.split('\t')
        print(ipaddr)
        data = dns.Record.list(api_session=api, data_old = ipaddr, target_is_singleton_old=True, target_is_reverse_unique_old=True)
        fqdn = data[0].fqdn
        fqdn_desc_old = data[0].fqdn_description
        if(args.append and fqdn_desc_old is not None):
            fqdn_description = fqdn_desc + ' # ' + fqdn_desc_old
        else:
            fqdn_description = fqdn_desc
        fqdns.append(
            dns.Fqdn.update_ta(
                value_old=fqdn,
                description_new=fqdn_description,
            )
        )
api.execute_ta(fqdns)
