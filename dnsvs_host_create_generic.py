#!/usr/bin/env python3

import argparse
import ipaddress
import netaddr

from netdb_client.api32 import dns, nd, APIEndpoint, APISession;
from netdb_client import util;

def check_cidr(api, cidr):
    res = nd.IpSubnet.list(
        api_session=api,
        cidr_old=cidr,
        is_container_old=False,
        is_routable_old=True,
        type_old='4'
    )
    if len(res) == 0:
        return False
    else:
        return True

def check_ipaddr_subnet(api, cidr, startip, endip):
    if ipaddress.ip_address(startip) in ipaddress.ip_network(cidr) and \
        ipaddress.ip_address(endip) in ipaddress.ip_network(cidr):
            return True
    else:
            return False

def check_ipaddr_range(api, startip, endip):
    if ipaddress.ip_address(startip) < ipaddress.ip_address(endip):
            return True
    else:
            return False

domain = "scc.kit.edu"

parser = util.ArgumentParser(description='new hosts generic')
parser.add_argument('--domain', '-d', nargs='?', default=domain, help='domain (default: {})'.format(domain))
parser.add_argument('--start', '-S', required=True, help='starting IP address')
parser.add_argument('--end', '-E', required=True, help='ending IP address')
parser.add_argument('--label', '-l', required=True, help='label')
parser.add_argument('--fqdn_descr', '-f', required=True, help='description text')
parser.add_argument('--reserve', action=argparse.BooleanOptionalAction, required=False, default=False, help='Reserve the address range as well')
parser.add_argument('subnet', help='subnet')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

cidr = args.subnet
res = check_cidr(api, cidr)
if(not res):
    raise ValueError("Subnet " + cidr + " does not exist.")

startip = args.start
endip = args.end
res = check_ipaddr_subnet(api, cidr, startip, endip)
if(not res):
    raise ValueError("Subnet " + cidr + " does not contain given ip address.")
res = check_ipaddr_range(api, startip, endip)
if(not res):
    raise ValueError("Ending IP address is less than starting IP address")

if (args.domain):
    domain = args.domain
label = args.label + "-"
fqdn_description=args.fqdn_descr

ip_list = list(netaddr.iter_iprange(startip, endip))

records = []
for i in ip_list:
    ipaddr = str(i)
    ip_str = str(ipaddr).replace(".", "-")
    fqdn = label + ip_str + "." + domain + "."
    records.append(
        dns.Record.create_ta(
            fqdn_new=fqdn,
            data_new=ipaddr,
            fqdn_description_new=fqdn_description,
            type_new="A",
        )
    )
if args.reserve:
    records.append(
        dns.IpAddr.bulk_update_ta(
            is_reserved_new=True,
            value_start_old=startip,
            value_stop_old=endip,
        )
    )
api.execute_ta(records)
