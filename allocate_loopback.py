#!/usr/bin/env python3
import ipaddress
from pprint import pprint
import re
import sys

from netdb_client.api32 import dns, APIEndpoint, APISession;
from netdb_client import util;

VRFS = {
	'service': 13,
	'backbone': 10,
	'fwbypass': 12,
	'border': 11,
	'tmn': 14,
	'test-backbone': 15,
	'ga': 16,
	'tk': 17,
	'hifis': 18,
	'sys-virt': 19,
	'wh-bb': 20,
	'glt-hw': 23
}

parser = util.ArgumentParser(description='allocate KIT-Core loopbacks')
parser.add_argument('vrf', help='name of VRF')
parser.add_argument('leaf_1_hostname', help='hostname of first leaf, not fqdn, e.g. rcs-2021-l-13-1')
parser.add_argument('leaf_2_hostname', help='hostname of second leaf, not fqdn, e.g. rcs-3050-l-13-2')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

# Find max allocated BCD number
BCD_NAME_REGEXP = "^loopback-kit-core-([0-9]{1,4})$"
loopback_bcds, mdl1, mdl2 = api.execute_ta([
	{"name": "nd.bcd.list", "old": {
		"name_regexp": "^loopback-kit-core-([0-9]{1,4})$"
	}},
	{"name": "nd.module.list", "old": {
		"fq_name": args.leaf_1_hostname
	}},
	{"name": "nd.module.list", "old": {
		"fq_name": args.leaf_2_hostname
	}}
])

max_allocated_number = 0
for existing_bcd in loopback_bcds:
	number = int(re.search(BCD_NAME_REGEXP, existing_bcd['name']).group(1))
	max_allocated_number = max(max_allocated_number, number)

if max_allocated_number % 2 == 0:
	print('Loopback number invalid')
	sys.exit(1)

if not args.vrf in VRFS:
	print(f'Invalid/unknown VRF {args.vrf}.')
	sys.exit(1)

# Check if we can find modules with the specified names
if len(mdl1) != 1:
	print(f'No NETDOC Module found for leaf {args.leaf_1_hostname}')
	sys.exit(1)

if len(mdl2) != 1:
	print(f'No NETDOC Module found for leaf {args.leaf_2_hostname}')
	sys.exit(1)

# Calc infos
loopback_interface_number = VRFS[args.vrf]

ipv4_base_address = ipaddress.ip_address('172.19.0.0')
ipv6_base_address = ipaddress.ip_address('2a00:1398:e::')

loopback_bcd_1_number = max_allocated_number + 1
loopback_bcd_2_number = max_allocated_number + 2

bcd_1_name = f'loopback-kit-core-{loopback_bcd_1_number}'
bcd_1_description = f'loopback{loopback_interface_number} for VRF {args.vrf} on {args.leaf_1_hostname}'

bcd_2_name = f'loopback-kit-core-{loopback_bcd_2_number}'
bcd_2_description = f'loopback{loopback_interface_number} for VRF {args.vrf} on {args.leaf_2_hostname}'

bcd_1_ipv4 = str(ipv4_base_address + loopback_bcd_1_number)
bcd_2_ipv4 = str(ipv4_base_address + loopback_bcd_2_number)

bcd_1_ipv6 = str(ipv6_base_address + loopback_bcd_1_number)
bcd_2_ipv6 = str(ipv6_base_address + loopback_bcd_2_number)

bcd_1_hostname = f'{args.leaf_1_hostname}-lo{loopback_interface_number}.scc.kit.edu.'
bcd_2_hostname = f'{args.leaf_2_hostname}-lo{loopback_interface_number}.scc.kit.edu.'

print(f'Maximum allocated loopback BCD number is {max_allocated_number}, using {loopback_bcd_1_number} and {loopback_bcd_2_number}')
print(f'Creating BCD {bcd_1_name}')
print(f'* {bcd_1_description}')
print(f'* IPv4: {bcd_1_ipv4}/32, IPv6: {bcd_1_ipv6}/128')
print(f'* Hostname: {bcd_1_hostname}')
print()
print(f'Creating BCD {bcd_2_name}')
print(f'* {bcd_2_description}')
print(f'* IPv4: {bcd_2_ipv4}/32, IPv6: {bcd_2_ipv6}/128')
print(f'* Hostname: {bcd_2_hostname}')
print()
print()

print('Router config:')
print(f'! {args.leaf_1_hostname}')
print(f'interface loopback{loopback_interface_number}')
print(f'  description vrf-{args.vrf}')
print(f'  vrf member {args.vrf}')
print(f'  ip address {bcd_1_ipv4}/32')
print(f'  ipv6 address {bcd_1_ipv6}/128')
print()
print(f'! {args.leaf_2_hostname}')
print(f'interface loopback{loopback_interface_number}')
print(f'  description vrf-{args.vrf}')
print(f'  vrf member {args.vrf}')
print(f'  ip address {bcd_2_ipv4}/32')
print(f'  ipv6 address {bcd_2_ipv6}/128')

print()
print()
print('Continue? [yn]')
res = input()
if res != 'y':
	print('Exiting.')
	sys.exit()


ta = []
ta.append({
    "name": "nd.bcd.create",
    "new": {
        "name": bcd_1_name,
        "categ": "RTR",
        "seclvl": 0,
        "description": bcd_1_description
    }
})
ta.append({
    "name": "nd.bcd2ou.create",
    "new": {
        "bcd_name": bcd_1_name,
        "ou_short_name": "SCC-NET",
    }
})
ta.append({
    "name": "nd.bcd2group.create",
    "new": {
        "bcd_name": bcd_1_name,
        "group_name":  "scc-net-routing"
    }
})
ta.append({
    "name": "nd.ip_subnet.create",
    "new": {
        "bcd": bcd_1_name,
        "cidr": f'{bcd_1_ipv4}/32',
        "create_ipaddrs": False,
        "auto_create_ip_address": True
    }
})
ta.append({
    "name": "nd.ip_subnet.create",
    "new": {
        "bcd": bcd_1_name,
        "cidr": f'{bcd_1_ipv6}/128',
        "create_ipaddrs": False,
        "auto_create_ip_address": True
    }
})
ta.append({
    "name": "dns.record.create",
    "new": {
        "fqdn": bcd_1_hostname,
        "data": bcd_1_ipv4,
        "type": "A"
    }
})
ta.append({
    "name": "dns.record.create",
    "new": {
        "fqdn": bcd_1_hostname,
        "data": bcd_1_ipv6,
        "type": "AAAA"
    }
})


ta.append({
    "name": "nd.bcd.create",
    "new": {
        "name": bcd_2_name,
        "categ": "RTR",
        "seclvl": 0,
        "description": bcd_2_description
    }
})
ta.append({
    "name": "nd.bcd2ou.create",
    "new": {
        "bcd_name": bcd_2_name,
        "ou_short_name": "SCC-NET",
    }
})
ta.append({
    "name": "nd.bcd2group.create",
    "new": {
        "bcd_name": bcd_2_name,
        "group_name":  "scc-net-routing"
    }
})
ta.append({
    "name": "nd.ip_subnet.create",
    "new": {
        "bcd": bcd_2_name,
        "cidr": f'{bcd_2_ipv4}/32',
        "create_ipaddrs": False,
        "auto_create_ip_address": True
    }
})
ta.append({
    "name": "nd.ip_subnet.create",
    "new": {
        "bcd": bcd_2_name,
        "cidr": f'{bcd_2_ipv6}/128',
        "create_ipaddrs": False,
        "auto_create_ip_address": True
    }
})
ta.append({
    "name": "dns.record.create",
    "new": {
        "fqdn": bcd_2_hostname,
        "data": bcd_2_ipv4,
        "type": "A"
    }
})
ta.append({
    "name": "dns.record.create",
    "new": {
        "fqdn": bcd_2_hostname,
        "data": bcd_2_ipv6,
        "type": "AAAA"
    }
})


api.execute_ta(ta)
