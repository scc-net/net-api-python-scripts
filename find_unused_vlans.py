#!/usr/bin/env python3
import pprint

from netdb_client.api32 import APIEndpoint, APISession
from netdb_client.util import ArgumentParser
from netdb_client.util import list_to_generator_map_one2many as o2m

parser = ArgumentParser()
parser.add_argument('--ignore', '-i', nargs='*', help='Net-Instances to ignore')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

ta = [
    {'idx': 'vlan', 'name': 'nd.vlan.list'},
    {'idx': 'vlan2device', 'name': 'nd.vlan2device.list', 'join': {'vlan': 'default'}},
    {'idx': 'reserved_bcd', 'name': 'nd.bcd.list', 'old': {'name_list': ['hardware_reserved', 'scc_net_reserved_vlan']}},
    {'idx': 'reserved_vlan', 'name': 'nd.vlan.list', 'join': {'reserved_bcd': 'default'}}
]

res = api.execute_ta(ta, dict_mode=True)

vlan_gpk2dev = dict(o2m(res['vlan2device'], 'vlan_gfk'))
reserved_vlans = [(vlan['id'], vlan['name'], vlan['net_instnc']) for vlan in res['reserved_vlan']]

vlan2device_map = {}
for vlan in res['vlan']:
    gpk = vlan['gpk']

    if gpk in vlan_gpk2dev:
        devices = [dev['dev_fqdn'] for dev in vlan_gpk2dev[gpk]]
    else:
        devices = []

    vlan2device_map[(vlan['id'], vlan['name'], vlan['net_instnc'])] = devices

print("VLANs not configured on our Routers / Switches:")
for vlan, devices in vlan2device_map.items():
    if vlan in reserved_vlans:
        continue
    if args.ignore and vlan[2] in args.ignore:
        continue
    if len(devices) == 0:
        print(f'  - {vlan[1]} ({vlan[0]} / {vlan[2]})')
