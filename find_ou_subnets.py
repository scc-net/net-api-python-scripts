#!/usr/bin/env python3
import json
from typing import Any

import netdb_client.util
from netdb_client.api41 import APIEndpoint, APISession
from netdb_client.util import ArgumentParser

parser = ArgumentParser(description='Search for all subnets')
parser.add_argument('ou', metavar='OU', type=str, help='OU short name')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

ta = [
    {'idx': 'ou', 'name': 'org.unit.list', 'old': {'short_name': args.ou}},
    {'idx': 'sub-ou', 'name': 'org.unit.list', 'inner_join_ref': {'ou': 'api_func_org_unit_hierarchy_gfk_list_is_superset'}},
    {'idx': 'bcd2ou', 'name': 'nd.bcd2ou.list', 'union_join_ref': {'ou': 'default', 'sub-ou': 'default'}},
    {'idx': 'bcd', 'name': 'nd.bcd.list', 'inner_join_ref': {'bcd2ou': 'default'}},
    {'idx': 'subnets', 'name': 'nd.ip_subnet.list', 'inner_join_ref': {'bcd': 'default'}},
]

res = api.execute_ta(ta, dict_mode=True)
bcd_map: dict[str, dict[str, Any]] = dict(netdb_client.util.list_to_generator_map_one2one(res['bcd'], 'name'))

bcds_subnets: dict[str, dict[str, Any]] = dict()
for subnet in res['subnets']:
    bcd = subnet['bcd']
    if bcd not in bcds_subnets:
        bcds_subnets[bcd] = {'category': bcd_map[bcd]['categ']}
    if 'subnets' not in bcds_subnets[bcd]:
        bcds_subnets[bcd]['subnets'] = []
    bcds_subnets[bcd]['subnets'].append(subnet['cidr'])
    bcds_subnets[bcd]['subnets'].sort()

print(json.dumps(dict(sorted(bcds_subnets.items())), indent=2))
