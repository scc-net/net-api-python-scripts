#!/usr/bin/env python3

import itertools
import sys
import termios

from netdb_client.api41 import dns, nd, APIEndpoint, APISession;
from netdb_client import util;

def yesno(question, choices=((('y','yes','ja','j','oui'),True),(('n', 'no','n','nein'),False)), default_choice=None, retries=None):
    prompt_choices = [c[0][0] for c in choices]
    if default_choice is not None:
        prompt_choices[default_choice] = prompt_choices[default_choice].upper()
    prompt = "{} [{}] ".format(question, "/".join(prompt_choices))
    if retries is None:
        retry_condition = itertools.count(1)
    else:
        retry_condition = range(0, retries)
    for _ in  retry_condition:
        termios.tcflush(sys.stdin, termios.TCIFLUSH);
        choice = input(prompt).lower()
        if default_choice is not None and choice == '':
            return choices[default_choice][1]
        for c in choices:
            if choice in c[0]:
                return c[1]
        else:
            print('invalid selection!')
    else:
        raise NotImplementedError


parser = util.ArgumentParser(description='Delete BCD')
parser.add_argument('bcd', help='BCD which should be deleted')
#parser.add_argument('-r', action="store_true", default=False, help='delete objects depending on BCD')
parser.add_argument('-y', action="store_true", default=False, help='Do not ask')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
session = APISession(endpoint)

ta_list = []
ta_list.append({"name": "nd.bcd.list", "old": {"name": args.bcd}})
ta_list.append({"name": "nd.vlan.list", "inner_join_ref": {0: "default"}})
ta_list.append({"name": "nd.vxlan.list", "inner_join_ref": {0: "default"}})
ta_list.append({"name": "nd.ip_subnet.list", "inner_join_ref": {0: "default"}})
ta_list.append({"name": "dns.ip_addr.list", "inner_join_ref": {3: "default"}})
ta_list.append({"name": "dns.record.list", "inner_join_ref": {4: "default"}})

result_list = session.execute_ta(ta_list)

if not result_list[0]:
    print("BCD {bcd} not found.".format(bcd=args.bcd))
    sys.exit(1)
else:
    bcd = result_list[0][0]
    print("# BCD\nname: {bcd[name]}\nCategory: {bcd[categ]}\nDescription: {bcd[admin_description]}\nAdmin Description: {bcd[admin_description]}\n".format(bcd=bcd))

if result_list[1]:
    print('# VLAN')
    for vlan in result_list[1]:
        print("Name: {vlan[name]} ID: {vlan[id]} Instance: {vlan[net_instnc]}".format(vlan=vlan))
    print()

if result_list[2]:
    print('# VXLAN')
    print("VNI: {vxlan[vni]}".format(vxlan=result_list[2][0]))
    print()

if result_list[3]:
    print('# Subnet')
    for subnet in result_list[3]:
        print("Subnet: {subnet[cidr]} Descripton: {subnet[description]}".format(subnet=subnet))
    print()

if result_list[5]:
    print('# DNS Resource Records')
    for rr in result_list[5]:
        print("{rr[fqdn]} {rr[type]} {rr[data]}".format(rr=rr))
    print()

if not yesno("Delete listed objects?", default_choice=1):
    sys.exit(1)

ta_delete = []
for vlan in result_list[1]:
    ta_delete.append(nd.Vlan.delete_ta(gpk_old=vlan['gpk']))
if result_list[2]:
    ta_delete.append(nd.Vxlan.delete_ta(vni_old=result_list[2][0]['vni']))
for subnet in result_list[3]:
    ta_delete.append(nd.IpSubnet.delete_ta(cidr_old=subnet['cidr'], delete_dns_rrs_old=True, delete_ipaddrs_old=True))
ta_delete.append(nd.Bcd.delete_ta(name_old=result_list[0][0]['name']))

session.execute_ta(ta_delete)
