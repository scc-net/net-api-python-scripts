#!/usr/bin/env python3

from netdb_client.api32 import dns, APIEndpoint, APISession;
from netdb_client import util;

parser = util.ArgumentParser(description='delete resource records')
parser.add_argument('file', help='file with rr entries: owner fqdn, dbrt, dest fqdn - separated by spaces')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

if args.file:
    filename = args.file
    lines = [line.strip() for line in open(filename)]

records = []
for line in lines:
    [fqdn, dbrt, data] = line.split("\t")
    ta_element = dns.Record.delete_ta(
        fqdn_old=fqdn,
        data_old=data,
        type_old=dbrt,
    )
    records.append(ta_element)

api.execute_ta(records)
