#!/usr/bin/env python3
import csv
import json
from argparse import FileType

import tabulate
from netdb_client.api41 import APIEndpoint, APISession
from netdb_client.util import ArgumentParser, list_to_generator_map_one2many, list_to_generator_map_one2one

def gather_ou_mgrs(root, p_by_gpk, m_by_ou):
    l = list()
    parent = p_by_gpk.get(root['parent_gfk'], None)
    if parent is None:
        return []
    l.extend(m_by_ou.get(parent['short_name'], []))
    l.extend(gather_ou_mgrs(parent, p_by_gpk, m_by_ou))
    return l

parser = ArgumentParser(description='Find all NETVS users assigned to a list of BCDs via group assignments.')
parser.add_argument('--massmail', type=FileType('w'), help='File to write data in KIT-CERT massmailer format')
parser.add_argument('--csv', type=FileType('w'), help='File to write data in CSV format')
parser.add_argument('--fallback-to-ou', action="store_true",
                    help='Fallback to OU if the bcd(s) do not have group assignments which resolve to a mgr.')
parser.add_argument('bcds', type=str, help='BCDs (, spereated)')
args = parser.parse_args()

bcds = [b.strip() for b in args.bcds.split(',')]
endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)
ta = [
    {"idx": "bcds", "name": "nd.bcd.list", "old": {"name_list": bcds}},
    {"idx": "bcd2group", "name": "nd.bcd2group.list", "inner_join_ref": {'bcds': 'default'}},
    {"idx": "groups", "name": "cntl.group.list", "inner_join_ref": {'bcd2group': 'default'}},
    {"idx": "mgr2groups", "name": "cntl.mgr2group.list", "inner_join_ref": {'groups': 'default'}},
    {"idx": "mgr_grp", "name": "cntl.mgr.list", 'old': {'kit_status': 1, 'is_sub': False, 'is_svc': False},
     'inner_join_ref': {'mgr2groups': 'default'}},
]
if args.fallback_to_ou:
    ta.append({"idx": "bcd2ou", "name": "nd.bcd2ou.list", "inner_join_ref": {"bcds": "default"}})
    ta.append({"idx": "ou", "name": "org.unit.list", "inner_join_ref": {"bcd2ou": "default"}})
    ta.append({"idx": "mgr2ou", "name": "cntl.mgr2ou.list", "inner_join_ref": {"ou": "default"}})
    ta.append({"idx": "mgr_ou", "name": "cntl.mgr.list", "inner_join_ref": {"mgr2ou": "default"}, 'old': {'kit_status': 1, 'is_sub': False, 'is_svc': False}})
    ta.append({
        "idx": 'parent_ous',
        "name": 'org.unit.list',
        "old": {"sorting_params_list": ['tree_level DESC']},
        "inner_join_ref": {"ou": 'api_func_org_unit_hierarchy_gfk_list_is_subset'}
      })
    ta.append({"idx": "mgr2ou_parents", "name": "cntl.mgr2ou.list", "inner_join_ref": {"parent_ous": "default"}})
    ta.append({"idx": "mgr_parent_ou", "name": "cntl.mgr.list", "inner_join_ref": {"mgr2ou_parents": "default"}, 'old': {'kit_status': 1, 'is_sub': False, 'is_svc': False}})

result = api.execute_ta(ta)
seen = dict()
users = []
usr = result[4]
mgr_to_group_by_mgr = dict(list_to_generator_map_one2many(result[3], 'mgr_login_name'))
bcd_to_group_by_group = dict(list_to_generator_map_one2many(result[1], 'group_name'))
bcd_to_group_by_bcd = dict(list_to_generator_map_one2many(result[1], 'bcd_name'))
bcds_seen_by_users = dict()

bcd_by_name = dict(list_to_generator_map_one2one(result[0], 'name'))
bcds_by_usr = dict()
for u in usr:
    if u['login_name'] in seen:
        continue
    bcds_by_usr[u['login_name']] = list()
    for g in mgr_to_group_by_mgr[u['login_name']]:
        for b in bcd_to_group_by_group[g['group_name']]:
            bcds_by_usr[u['login_name']].append(bcd_by_name[b['bcd_name']])
            bcds_seen_by_users[b['bcd_name']] = b
    seen[u['login_name']] = u
    users.append(u)
if args.fallback_to_ou:
    mgr_to_ou_by_ou = dict(list_to_generator_map_one2many(result[7], 'ou_short_name'))
    mgr_to_parent_ou_by_ou = dict(list_to_generator_map_one2many(result[10], 'ou_short_name'))
    bcd_to_ou_by_ou = dict(list_to_generator_map_one2many(result[5], 'ou_short_name'))
    bcd_to_ou_by_bcd = dict(list_to_generator_map_one2many(result[5], 'bcd_name'))
    mgrs_by_login_name = dict(list_to_generator_map_one2one(result[8], 'login_name'))
    parent_mgrs_by_login_name = dict(list_to_generator_map_one2one(result[11], 'login_name'))
    parent_ous_by_gpk = dict(list_to_generator_map_one2one(result[9], 'gpk'))
    parent_ous_by_short_name = dict(list_to_generator_map_one2one(result[9], 'short_name'))
    ous_by_short_name = dict(list_to_generator_map_one2one(result[6], 'short_name'))
    for b in bcds:
        if b in bcds_seen_by_users:
            continue
        # b is not seen by any group user, adding OU mgrs instead...
        for g in bcd_to_ou_by_bcd[b]:
            for m in mgr_to_ou_by_ou.get(g['ou_short_name'], []):
                if m['mgr_login_name'] not in bcds_by_usr:
                    bcds_by_usr[m['mgr_login_name']] = []
                bcds_by_usr[m['mgr_login_name']].append(bcd_by_name[b])
                if m['mgr_login_name'] not in seen:
                    if m['mgr_login_name'] not in mgrs_by_login_name:
                        continue
                    users.append(mgrs_by_login_name[m['mgr_login_name']])
                    seen[m['mgr_login_name']] = mgrs_by_login_name[m['mgr_login_name']]
            parent_ou_mgrs = gather_ou_mgrs(ous_by_short_name[g['ou_short_name']], parent_ous_by_gpk, mgr_to_parent_ou_by_ou)
            for m in parent_ou_mgrs:
                if m['mgr_login_name'] not in bcds_by_usr:
                    bcds_by_usr[m['mgr_login_name']] = []
                bcds_by_usr[m['mgr_login_name']].append(bcd_by_name[b])
                if m['mgr_login_name'] not in seen:
                    if m['mgr_login_name'] not in parent_mgrs_by_login_name:
                        continue
                    users.append(parent_mgrs_by_login_name[m['mgr_login_name']])
                    seen[m['mgr_login_name']] = parent_mgrs_by_login_name[m['mgr_login_name']]

users = [u for u in users if u['email'] is not None and u['email'] != '']
headers = ['Name', 'Account', 'E-Mail']
user_list = [
    [f'{user["first_name"]} {user["last_name"]}', user['login_name'], user['email']]
    for user in users]
print(f'Users assigned to groups which contain one of the BCDs "{args.bcds}":')
print(tabulate.tabulate(user_list, headers=headers))

if args.massmail is not None:
    cont = dict()
    cont['globals'] = {
        'additional_headers': {
            'Reply-To': 'netvs@scc.kit.edu',
            'X-Auto-Response-Suppress': 'All',
            'Precedence': 'Bulk',
            'Auto-Submitted': 'yes'
        }
    }
    cont['individual'] = list()
    for u in users:
        cont['individual'].append({
            'to': u['email'],
            'name': f'{u["first_name"]} {u["last_name"]}',
            'login_name': u['login_name'],
            'bcds': bcds_by_usr[u['login_name']],
        })
    args.massmail.write(json.dumps(cont, indent=4))
    print(f'Wrote massmail data into {args.massmail.name}')

if args.csv is not None:
    writer = csv.writer(args.csv)
    writer.writerow(headers)
    writer.writerows(user_list)
    print(f'Wrote CSV data into {args.csv.name}')
