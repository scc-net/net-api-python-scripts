#!/usr/bin/env python3

import ipaddress
import sys

from netdb_client.api32 import dns, APIEndpoint, APISession;
from netdb_client import util;

parser = util.ArgumentParser(description='Bulk Transfer BCD creation')
parser.add_argument('file', help='file containing tr bcd entries')
parser.add_argument('--group', '-G', required=True, help='group to use for BCDs')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

if not args.file:
    print("No input file given")
    sys.exit(1)

lines = [line.strip() for line in open(args.file)]

ta = []
for line in lines:
    if not line.strip():
        continue

    [bcd, hostname1, hostname2, ipv4_subnet, ipv6_subnet] = line.split()

    ipv4_network = ipaddress.ip_network(ipv4_subnet)

    if ipv4_network.prefixlen not in [31, 30]:
        print(f"Invalid Prefix Len for IPv4 Transfer network {ipv4_network}")
        sys.exit(1)

    ipv4_hosts = ipv4_network.hosts()
    ipv4_host1 = next(ipv4_hosts)
    ipv4_host2 = next(ipv4_hosts)

    ipv6_network = ipaddress.ip_network(ipv6_subnet)

    if ipv6_network.prefixlen == 64:
        ipv6_network = next(ipv6_network.subnets(new_prefix=127))

    if ipv6_network.prefixlen != 127:
        print(f"Invalid Prefix Len for IPv6 Transfer network {ipv6_network}")
        sys.exit(1)

    ipv6_hosts = ipv6_network.hosts()
    ipv6_host1 = next(ipv6_hosts)
    ipv6_host2 = next(ipv6_hosts)

    print(f"Creating BCD {bcd}")
    print(f"  Host 1: {ipv4_host1}, {ipv6_host1} -> {hostname1}")
    print(f"  Host 2: {ipv4_host2}, {ipv6_host2} -> {hostname2}")

    ta.append({
        "name": "nd.bcd.create",
        "new": {
            "name": bcd,
            "categ": "TR",
            "seclvl": 0,
            "ou_short_name": "SCC-NET"
        }
    })

    ta.append({
        "name": "nd.bcd2group.create",
        "new": {
            "bcd_name": bcd,
            "group_name": args.group
        }
    })

    ta.append({
        "name": "nd.ip_subnet.create",
        "new": {
            "bcd": bcd,
            "cidr": str(ipv4_subnet),
            "create_ipaddrs": False,
            "auto_create_ip_address": True
        }
    })
    ta.append({
        "name": "nd.ip_subnet.create",
        "new": {
            "bcd": bcd,
            "cidr": str(ipv6_subnet),
            "create_ipaddrs": False,
            "auto_create_ip_address": True
        }
    })

    ta.append({
        "name": "dns.record.create",
        "new": {
            "fqdn": hostname1 + ".",
            "data": str(ipv4_host1),
            "type": "A"
        }
    })

    ta.append({
        "name": "dns.record.create",
        "new": {
            "fqdn": hostname1 + ".",
            "data": str(ipv6_host1),
            "type": "AAAA"
        }
    })

    ta.append({
        "name": "dns.record.create",
        "new": {
            "fqdn": hostname2 + ".",
            "data": str(ipv4_host2),
            "type": "A"
        }
    })
    ta.append({
        "name": "dns.record.create",
        "new": {
            "fqdn": hostname2 + ".",
            "data": str(ipv6_host2),
            "type": "AAAA"
        }
    })

#import pprint
#pprint.pprint(ta)
api.execute_ta(ta)
