#!/usr/bin/env python3

from netdb_client.api32 import dns, APIEndpoint, APISession;
from netdb_client import util;

parser = util.ArgumentParser(description='new host')
group = parser.add_mutually_exclusive_group(required=False)
group.add_argument('-a', action="store_true", help='a record only')
group.add_argument('-aaaa', action="store_true", help='aaaa record only')
parser.add_argument('file', help='file containing host entries: fqdn [ipv4addr] [ipv6addr], separated by spaces')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

if args.file:
    filename = args.file
    lines = [line.strip() for line in open(filename)]

records = []
for line in lines:
    if args.a:
        [fqdn, ipv4addr] = line.split()
        records.append(
            dns.Record.create_ta(
                fqdn_new=fqdn,
                data_new=ipv4addr,
                type_new="A",
            )
        )
    elif args.aaaa:
        [fqdn, ipv6addr] = line.split()
        records.append(
            dns.Record.create_ta(
                fqdn_new=fqdn,
                data_new=ipv6addr,
                type_new="AAAA",
            )
        )
    else:
        [fqdn, ipv4addr, ipv6addr] = line.split()
        records.append(
            dns.Record.create_ta(
                fqdn_new=fqdn,
                data_new=ipv4addr,
                type_new="A",
            )
        )
        records.append(
            dns.Record.create_ta(
                fqdn_new=fqdn,
                data_new=ipv6addr,
                type_new="AAAA",
            )
       )

api.execute_ta(records)
