#!/usr/bin/env python3
import csv
import json
from argparse import FileType

import tabulate
from dns.name import from_unicode, NoParent
from netdb_client.api32 import APIEndpoint, APISession
from netdb_client.util import ArgumentParser

parser = ArgumentParser(description='Find all users managing records in a specific zone.')
parser.add_argument('zones', type=str, help='zones (, spereated)')
parser.add_argument('--massmail',  type=FileType('w'), help='File to write data in KIT-CERT massmailer format')
parser.add_argument('--csv', type=FileType('w'), help='File to write data in CSV format')
args = parser.parse_args()

ou_types_not = ['DEK', 'Fsch', 'StudWH(SW)', 'StudWH(andere)', 'StudOrg', 'KITVerbundEinr', 'FremdEinr', 'VS', 'HSG' ]
ous = {}
endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

for z in args.zones.split(','):
    zone = from_unicode(z)

    ta = [
        {'idx': 'fqdn', 'name': 'dns.fqdn.list', 'old': {'zone': str(zone), 'has_rr': True}},
        {"idx": "records", 'name': 'dns.record.list', 'join': {'fqdn': 'default'}}
    ]
    result = api.execute_ta(ta)
    name_list = []
    for r in result[1]:
        if r['target_bcd_list'] is None:
            continue
        name_list += r['target_bcd_list']
    name_list = list({t: t for t in name_list}.values()) 
    for n in name_list:
        ta = [
            {"idx": "bcds", 'name': 'nd.bcd.list', 'old': {'name': n}},
            {"idx": "ou2bcd", "name": "nd.bcd2ou.list", "join": {"bcds": "default"}},
            {"idx": "ous", "name": "org.unit.list", "join": {"ou2bcd": "default"}},
        ]

        result = api.execute_ta(ta)
        data = result[2][0]
        if(data['type'] not in ou_types_not):
            ous[data['short_name']] = data['name']

if args.massmail is not None:
    cont = dict()
    cont['globals'] = {
        'key' : '/var/tmp/klara-key.pem',
        'cert' : '/var/tmp/klara-cert.pem',
        'from_name': 'Klara Mall',
        'from' : 'klara.mall@kit.edu',
        'bcc' :
                [
                        'klara.mall@kit.edu'
                ],
        'subject': 'Info-L3 Abkündigung Altdomains fzk.de, uni-karlsruhe.de, uka.de',
        'additional_headers': {
            'X-Auto-Response-Suppress': 'All',
            'Precedence': 'Bulk',
            'Auto-Submitted': 'yes'
        }
    }
    cont['individual'] = list()
    for ou in sorted(ous):
        print(ou + '\t' + ous[ou])
        cont['individual'].append({
            'to': 'leitung@' + ou + '.kit.edu',
            'ou_short': ou
            })
    args.massmail.write(json.dumps(cont, ensure_ascii=False, indent=4))
    print(f'Wrote massmail data into {args.massmail.name}')
