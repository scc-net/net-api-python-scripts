#!/usr/bin/env python3

import re
import struct
from socket import inet_aton

from netdb_client.api32 import dns, APIEndpoint, APISession;
from netdb_client import util;

def gen_leases_list(dbapi, bcd):
    ipaddrs = []
    mac = {}
    data = dns.Record.list(api_session=api, type_old="A", target_bcd_list_old=[bcd], target_is_singleton_old=True, target_is_reverse_unique_old=True)
    for a in data:
        desc = a.fqdn_description
        if(desc):
            if(re.search(r'^([0-9A-F]{2}:){5}([0-9A-F]{2})', desc, re.I)):
                m = re.search(r'^([0-9A-F]{2}:){5}([0-9A-F]{2})', desc, re.I).group()
                ipaddr = a.data
                ipaddrs.append(ipaddr)
                if (ipaddr not in mac):
                    mac[ipaddr] = []
                mac[ipaddr].append(m.lower())

    sorted_list_of_ips = sorted(ipaddrs, key=lambda ip: struct.unpack('!L', inet_aton(ip))[0])

    for i in sorted_list_of_ips:
        for m in mac[i]:
            #print('\t'.join([i, m]))
            print(m)


parser = util.ArgumentParser(description='dhcp: get list of mac adresses in static leases')
parser.add_argument('bcd', help='bcd')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

bcd = args.bcd
gen_leases_list(api, bcd)
