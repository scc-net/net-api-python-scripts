#!/usr/bin/env python3

from netdb_client.api32 import dns, APIEndpoint, APISession;
from netdb_client import util;

parser = util.ArgumentParser(description='delete fqdns')
parser.add_argument('file', help='file with fqdn entries')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

if args.file:
    filename = args.file
    lines = [line.strip() for line in open(filename)]

fqdns = []
for line in lines:
    if line.strip():
        fqdn = line
        fqdns.append(
            dns.Fqdn.delete_ta(
                value_old=fqdn,
            )
        )

api.execute_ta(fqdns)
