#!/usr/bin/env python3
from netdb_client.api32 import APIEndpoint, APISession
from netdb_client.util import ArgumentParser


parser = ArgumentParser(description='Find all FQDNs on which a user has access.')
parser.add_argument('username', type=str, help='Username')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

ta = [
    {"idx": "mgr", "name": "cntl.mgr.list", "old": {"login_name_list": [args.username]}},
    {"idx": "mgr2group", "name": "cntl.mgr2group.list", "join": {"mgr": "default"}},
    {"idx": "mgr2ou", "name": "cntl.mgr2ou.list", "join": {"mgr": "default"}},
    {"idx": "groups", "name": "cntl.group.list", "join": {"mgr2group": "default"}},
    {"idx": "ous", "name": "org.unit.list", "join": {"mgr2ou": "default"}},
    {"idx": "group2fqdn", "name": "dns.fqdn2group.list", "join": {"groups": "default"}},
    {"idx": "ou2fqdn", "name": "dns.fqdn2ou.list", "join": {"ous": "default"}},
]

result = api.execute_ta(ta, dict_mode=True)

group_fqdns = [entry["fqdn_value"] for entry in result["group2fqdn"]]
ou_fqdns = [entry["fqdn_value"] for entry in result["ou2fqdn"]]

fqdns = sorted(list(set(group_fqdns).union(set(ou_fqdns))))

print(fqdns)
