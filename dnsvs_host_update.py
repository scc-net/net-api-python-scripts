#!/usr/bin/env python3

from netdb_client.api32 import dns, APIEndpoint, APISession;
from netdb_client import util;

parser = util.ArgumentParser(description='update a/ptr record for a host')
group = parser.add_mutually_exclusive_group(required=False)
group.add_argument('-aaaa', action="store_true", help='aaaa instead of a')
parser.add_argument('file', help='file containing host entries: fqdn [ipaddr-old] [ipaddr-new], separated by spaces')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

if args.file:
    filename = args.file
    lines = [line.strip() for line in open(filename)]

rrtype = 'A'
if (args.aaaa):
    rrtype = 'AAAA'

records = []
for line in lines:
    [fqdn, ipaddr_old, ipaddr_new] = line.split()
    records.append(
       dns.Record.update_ta(
                fqdn_old=fqdn,
                data_old=ipaddr_old,
                data_new=ipaddr_new,
                type_old=rrtype,
        )
    )

api.execute_ta(records)
