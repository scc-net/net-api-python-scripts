#!/usr/bin/env python3
from netdb_client.api32 import APIEndpoint, APISession
from netdb_client.util import ArgumentParser

parser = ArgumentParser(
    description='Sanity check all gw-records'
)
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

gateway_fqdn_tmpl = 'defgw-v{vni}.{domain}'

incorrect_fqdns = {}
missing_records = set()
missing_vnis = []

fqdn_ta = [
    {'idx': 'fqdns', 'name': 'dns.fqdn.list', 'old': {'value_regexp': r'defgw.*\.scc\.kit\.edu'}},
    {'idx': 'rr', 'name': 'dns.record.list', 'join': {'fqdns': 'default'}},
    {'idx': 'ip_addr', 'name': 'dns.ip_addr.list', 'join': {'rr': 'default'}},
    {'idx': 'ip_sn', 'name': 'nd.ip_subnet.list', 'old': {'cidr_operator': 'smallest_cts'}, 'join': {'ip_addr': 'default'}},
    {'idx': 'bcds', 'name': 'nd.bcd.list', 'join': {'ip_sn': 'default'}},
    {'idx': 'vnis', 'name': 'nd.vxlan.list', 'join': {'bcds': 'default'}}
]
fqdn_res = api.execute_ta(fqdn_ta)

fqdn2bcd = {fqdn['value']: fqdn['rr_chain_target_bcd_list'] for fqdn in fqdn_res[0]}
bcd2vni = {vxlan['bcd']: vxlan['vni'] for vxlan in fqdn_res[5]}
ip_addr_not_reserved = {ip['value']: ip['cidr'] for ip in fqdn_res[2] if not ip['is_reserved']}

for fqdn, bcd_list in fqdn2bcd.items():
    if len(bcd_list) != 1:
        print(f"Weird stuff for fqdn {fqdn} (has 2 bcds: {bcd_list})")
    else:
        bcd = bcd_list[0]

        if bcd not in bcd2vni:
            missing_vnis.append(bcd)
            continue

        vni = bcd2vni[bcd]
        fqdn_labels = fqdn.split('.')
        domain = '.'.join(fqdn_labels[1:])
        correct_fqdn = gateway_fqdn_tmpl.format(vni=vni, domain=domain)
        if fqdn != correct_fqdn and not any(s in fqdn for s in ['-stby']):
            incorrect_fqdns[fqdn] = correct_fqdn

vni_ta = [
    {'idx': 'vnis', 'name': 'nd.vxlan.list'},
    {'idx': 'bcds', 'name': 'nd.bcd.list', 'join': {'vnis': 'default'}},
    {'idx': 'subnets', 'name': 'nd.ip_subnet.list', 'join': {'bcds': 'default'}},
    {'idx': 'ip_addr', 'name': 'dns.ip_addr.list', 'join': {'subnets': 'api_func_dns_ip_addr_is_subnet_defgw'}},
    {'idx': 'rrs', 'name': 'dns.record.list', 'join': {'ip_addr': 'default'}}
]
vni_res = api.execute_ta(vni_ta)

gw2bcd = {subnet['default_gateway']: subnet['bcd'] for subnet in vni_res[2]}
bcd2vni = {vxlan['bcd']: vxlan['vni'] for vxlan in vni_res[0]}
ip2fqdn = {record['data']: record['fqdn'] for record in vni_res[4]}
ip_addr_not_reserved.update({ip['value']: ip['cidr'] for ip in vni_res[3] if not ip['is_reserved']})

for gw, bcd in gw2bcd.items():
    if gw not in ip2fqdn:
        missing_records.add(bcd)
        continue

    vni = bcd2vni[bcd]
    fqdn = ip2fqdn[gw]
    fqdn_labels = fqdn.split('.')
    domain = '.'.join(fqdn_labels[1:])
    correct_fqdn = gateway_fqdn_tmpl.format(vni=vni, domain=domain)
    if fqdn != correct_fqdn:
        incorrect_fqdns[fqdn] = correct_fqdn

print('\nBCDs with defgw RR but without VNI:')
for bcd in missing_vnis:
    print(f'  - {bcd}')

print('\nNot reserved GW-IPs:')
for ip in ip_addr_not_reserved:
    print(f'  - {ip}')

print('\nIncorrect fqdns (fqdn: correct_fqdn):')
for wrong, correct in incorrect_fqdns.items():
    print(f'  {wrong}: {correct}')

print('\nBCDs with gateways in NETDOC but no FQDNS:')
for bcd in missing_records:
    print(f'  - {bcd}')
