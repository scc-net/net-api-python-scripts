#!/usr/bin/env python3

import ipaddress
import fileinput

from netdb_client.api32 import APIEndpoint, APISession
import netdb_client.util
import netdb_client.dns

parser = netdb_client.util.ArgumentParser(description='get cnames')
#parser.add_argument('bcd', help='bcd')
args = parser.parse_args()

#bcd = args.bcd

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

fqdn = 'data.atmohub.kit.edu'
record_data = netdb_client.dns.Record.list(api_session=api, fqdn_list_old = [fqdn], type_old='CNAME')
if (len(record_data) != 0):
    record = record_data[0]
#    print(record.fqdn + "\t" + record.type + "\t" + record.data + "\t" + record.target_bcd_list)
    print(record.target_bcd_list)

domain = fqdn.split('.')
del domain[0]
print('.'.join(domain))
