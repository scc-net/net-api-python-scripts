#!/usr/bin/env python3

from netdb_client.api32 import dns, APIEndpoint, APISession;
from netdb_client import util;

parser = util.ArgumentParser(description='update fqdns')
parser.add_argument('file', help='file containing fqdn entries: fqdn-old fqdn-new, separated by spaces')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

if args.file:
    filename = args.file
    lines = [line.strip() for line in open(filename)]

fqdns = []
for line in lines:
    if line.strip():
        [fqdn_old, fqdn_new] = line.split()
        fqdns.append(
            dns.Fqdn.update_ta(
                value_old=fqdn_old,
                value_new=fqdn_new,
            )
        )

api.execute_ta(fqdns)
