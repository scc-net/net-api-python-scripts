#!/usr/bin/env python3

import regex
from netdb_client.api32 import APIEndpoint, APISession
import netdb_client.util
import netdb_client.dns

prog_description = """search and replace of fqdn labels

Example: dnsvs_fqdn_replacy.py --dry-run "^(r|s)g-.*" "(.*)(-c01-)(.*)" "\\1-c02-\\3"
"""

parser = netdb_client.util.ArgumentParser(description=prog_description)
parser.add_argument('--dry-mode', default=False, action="store_true", help='perform a dry run')
parser.add_argument('fqdn', help='pattern to find fqdns')
parser.add_argument('search', help='pattern to search in label')
parser.add_argument('replace', help='pattern to replace matches in label')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

fqdn_regex = args.fqdn
search_pattern = regex.compile(args.search)
replace_pattern = args.replace
dry_mode = args.dry_mode

record_data = netdb_client.dns.Fqdn.list(api_session=api, value_regexp_old=args.fqdn)

ta = []
for record in record_data:
    old_fqdn = record.value
    description = ''
    if record.description is not None:
        description = f" ({record.description})"
    new_fqdn = regex.sub(search_pattern, replace_pattern, old_fqdn)
    if new_fqdn != old_fqdn:
        print(f"{old_fqdn} -> {new_fqdn}{description}")
        ta.append(netdb_client.dns.Fqdn.update_ta(value_old=old_fqdn, value_new=new_fqdn))

api.execute_ta(ta, dry_mode=dry_mode)
