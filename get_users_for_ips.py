#!/usr/bin/env python3
import csv
import fileinput
import ipaddress
import json
import pathlib
import sys
from argparse import FileType

import tabulate
from netdb_client.api41 import APIEndpoint, APISession
from netdb_client.util import ArgumentParser

parser = ArgumentParser(description='Find all users managing IPs.')
parser.add_argument(
    'ips',
    type=str,
    nargs='*',
    help='IPs or CIDRs. Can be given as multiple arguments, '
    'each argument can have multiple IPs or CIDRs separated by commas. '
    'If omitted, read lines from stdin. If an existing filename, read from that file.',
)
parser.add_argument('--massmail', type=FileType('w'), help='File to write data in KIT-CERT massmailer format')
parser.add_argument('--csv', type=FileType('w'), help='File to write data in CSV format')
parser.add_argument('--progress', action='store_true', help='Show progress')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

iterable = args.ips
if not iterable:
    iterable = fileinput.input('-')
elif len(iterable) == 1:
    ips_path = pathlib.Path(args.ips[0])
    if ips_path.is_file():
        with ips_path.open('r') as fh:
            iterable = fh.readlines()

# Flatten list
ips_list = []
for entry in iterable:
    if ',' in entry:
        ips_list.extend(entry.strip().split(','))
    else:
        ips_list.append(entry.strip())

if not ips_list:
    print('Failed to find working iterable IP/CIDR list', file=sys.stderr)
    sys.exit(1)

user_bcds_and_ips = {}
for idx, ip in enumerate(ips_list):
    if args.progress:
        print(f'Checking IP {ip:15} [{idx + 1}/{len(ips_list)}]', end='\r')

    ta_ip = [
        {'idx': 'ip_subnet', 'name': 'nd.ip_subnet.list', 'old': {'cidr': ip, 'cidr_operator': 'smallest_cts'}},
    ]
    try:
        ipaddress.ip_address(ip)
        ta_ip.append({'idx': 'record', 'name': 'dns.record.list', 'old': {'data': ip}})
    except ValueError:
        pass
    result_ip = api.execute_ta(ta_ip, dict_mode=True)
    if len(result_ip['ip_subnet']) == 0:
        print(f'Warning: No BCD found for ip {ip}!', file=sys.stderr)
        continue
    if len(result_ip['ip_subnet']) > 1:
        print(f'Warning: Ambiguous result for ip {ip}!', file=sys.stderr)
        continue

    ta_mgr = [
        {'idx': 'bcds', 'name': 'nd.bcd.list', 'old': {'name': result_ip['ip_subnet'][0]['bcd']}},
        {'idx': 'group2bcd', 'name': 'nd.bcd2group.list', 'inner_join_ref': {'bcds': 'default'}},
        {'idx': 'ou2bcd', 'name': 'nd.bcd2ou.list', 'inner_join_ref': {'bcds': 'default'}},
        {'idx': 'groups', 'name': 'cntl.group.list', 'inner_join_ref': {'group2bcd': 'default'}},
        {'idx': 'ous', 'name': 'org.unit.list', 'inner_join_ref': {'ou2bcd': 'default'}},
        {'idx': 'mgr2group', 'name': 'cntl.mgr2group.list', 'inner_join_ref': {'groups': 'default'}},
        {'idx': 'mgr2ou', 'name': 'cntl.mgr2ou.list', 'inner_join_ref': {'ous': 'default'}},
        {
            'idx': 'mgr_grp',
            'name': 'cntl.mgr.list',
            'old': {'is_sub': False, 'is_svc': False},
            'inner_join_ref': {'mgr2group': 'default'},
        },
        {
            'idx': 'mgr_ou',
            'name': 'cntl.mgr.list',
            'old': {'is_sub': False, 'is_svc': False},
            'inner_join_ref': {'mgr2ou': 'default'},
        },
    ]

    result_mgr = api.execute_ta(ta_mgr, dict_mode=True)
    users = result_mgr['mgr_grp'] + result_mgr['mgr_ou']
    for u in users:
        if u['login_name'] not in user_bcds_and_ips:
            user_bcds_and_ips[u['login_name']] = {'user': u, 'bcds': {}, 'ips': {}}
        user_bcds_and_ips[u['login_name']]['bcds'][result_mgr['bcds'][0]['name']] = result_mgr['bcds'][0]
        user_bcds_and_ips[u['login_name']]['ips'][ip] = {'ip': ip}
        if 'record' in result_ip:
            user_bcds_and_ips[u['login_name']]['ips'][ip]['fqdn'] = (
                result_ip['record'][0].get('fqdn', None) if result_ip['record'] else None
            )

headers = ['Name', 'Account', 'E-Mail', 'IPs', 'BCDs']
user_list = [
    [
        f'{user["user"]["first_name"]} {user["user"]["last_name"]}',
        user['user']['login_name'],
        user['user']['email'],
        ','.join([str(d) for d in user['ips'].values()]),
        ','.join([b['name'] for b in user['bcds'].values()]),
    ]
    for user in user_bcds_and_ips.values()
]

ips_list_str = ','.join(ips_list)
print(f'Users managing one or multiple of the ips "{ips_list_str}":')
print(tabulate.tabulate(user_list, headers=headers))

if args.massmail is not None:
    cont = {
        'globals': {
            'additional_headers': {
                'Reply-To': 'netvs@scc.kit.edu',
                'X-Auto-Response-Suppress': 'All',
                'Precedence': 'Bulk',
                'Auto-Submitted': 'yes',
            }
        },
        'individual': [],
    }
    for u in user_bcds_and_ips.values():
        cont['individual'].append(
            {
                'to': u['user']['email'],
                'name': f'{u["user"]["first_name"]} {u["user"]["last_name"]}',
                'login_name': u['user']['login_name'],
                'bcds': list(u['bcds'].values()),
                'ips': list(u['ips'].values()),
            }
        )
    args.massmail.write(json.dumps(cont, indent=4))
    print(f'Wrote massmail data into {args.massmail.name}')

if args.csv is not None:
    writer = csv.writer(args.csv)
    writer.writerow(headers)
    writer.writerows(user_list)
    print(f'Wrote data into {args.csv.name}')
