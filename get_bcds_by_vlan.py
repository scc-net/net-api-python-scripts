#!/usr/bin/env python3

from netdb_client.api32 import APIEndpoint, APISession;
from netdb_client import util;

parser = util.ArgumentParser(description='Get device,port,bulding,room,dst_mod for a given ingress vlan.')
parser.add_argument('--net-cmpnd', help='The net compound of the VLAN', dest='netcmpnd', required=True)
parser.add_argument('vlan', nargs='+', type=int, help='The VLAN id')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

netcmpnd = args.netcmpnd
vlans = args.vlan

ta_data = api.execute_ta([
    # find net cmpnd
    {"idx": "cmpnds", "name": "ndcfg.net_cmpnd.list", "old": {"name": netcmpnd}},
    {"idx": "c2i", "name": "ndcfg.net_cmpnd2instnc.list", "join": {"cmpnds": "default"}},
    {"idx": "instncs", "name": "ndcfg.net_instnc.list", "join": {"c2i": "default"}},
    {"idx": "vlans", "name": "nd.vlan.list", "old": {"id_list": vlans}, "join": {"instncs": "default"}},
    {"idx": "bcds", "name": "nd.bcd.list", "join": {"vlans": "default"}},
])

_, _, _, vlans, bcds = ta_data

assert len(vlans) == len(bcds)

print(" ".join((bcd['name'] for bcd in bcds)))
