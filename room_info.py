#!/usr/bin/env python3
import csv
from typing import Optional
from dataclasses import dataclass
from pprint import pprint
import sys

from netdb_client.api32 import dns, APIEndpoint, APISession;
from netdb_client import util;
from netdb_client.util import list_to_generator_map_one2one as o2o
from netdb_client.util import list_to_generator_map_one2many as o2m

parser = util.ArgumentParser(description='Get DD Room info')
parser.add_argument('room', nargs='*', help='building:room combination(s)')
parser.add_argument('--csv', type=str, help='CSV file to load Rooms from')
parser.add_argument('--csv-out', type=str, help='CSV file to write room info to. Will have the following columns: building, row, local module name, local p-port name, connected module name, connected p-port name, connected device fqdn, connected l-port name, vlan id, vlan netinstance, vlan name, bcd')
parser.add_argument('--csv-filter-vlan', type=str, help='filter VLAN in CSV, syntax: vlanid@netinstance')
parser.add_argument('--verbose', action='store_true', help='Enable Verbose output')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

@dataclass
class RoomDDInfo:
    building: str
    room: str
    local_module_name: str
    local_p_port_name: str
    connected_module_name: Optional[str] = None
    connected_p_port_name: Optional[str] = None
    connected_device_fqdn: Optional[str] = None
    connected_l_port_name: Optional[str] = None
    vlan_id: Optional[int] = None
    vlan_netinstance: Optional[str] = None
    vlan_name: Optional[str] = None
    bcd: Optional[str] = None

    def to_csv(self):
        return [
            self.building, self.room, self.local_module_name,
            self.local_p_port_name, self.connected_module_name,
            self.connected_p_port_name, self.connected_device_fqdn,
            self.connected_l_port_name, str(self.vlan_id),
            self.vlan_netinstance, self.vlan_name,
            self.bcd
        ]

ALLOWED_P_PORT_TYPE_GROUPS = [
    "Festkabel/TP/ETH",
    "Festkabel/LWL/2*MM",
    "Festkabel/LWL/2*SM",
    "Festkabel/LWL/URM/2*SM"
]

filter_vlan = None
if args.csv_filter_vlan:
    vlan_id, vlan_netinstnc = args.csv_filter_vlan.split('@', 1)
    res = api.execute_ta([
        {"name": "nd.vlan.list", "old": {"id": int(vlan_id), "net_instnc": vlan_netinstnc}}
    ])

    if len(res[0]) != 1:
        print(f"VLAN {vlan_id}@{vlan_netinstnc} not found or ambiguous.")
        sys.exit(1)

    filter_vlan = res[0][0]

all_rooms = []
for room in args.room:
    bldg, room = room.split(':', 1)
    all_rooms.append((bldg, room))

if args.csv:
    with open(args.csv, newline='') as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            bldg, room = row
            all_rooms.append((bldg, room))

csvrows = []
for bldg, room in all_rooms:
    res = api.execute_ta([
        {"name": "nd.room.list", "old": {"bldg": bldg, "number": room}},
        {"name": "ndcfg.module_type_class.list", "old": {"name": "DD"}},
        {"name": "ndcfg.module_type.list", "join": {"1": "api_fkey_ndcfg_module_type_class"}},
        {"name": "nd.module.list", "join": {"0": "api_fkey_nd_module_room", "2": "api_fkey_nd_module_type"}},
        {"name": "nd.p_port.list", "join": {"3": "api_fkey_nd_p_port_mdl_loc"}},
        {"name": "ndcfg.module_type_class.list", "old": {"name": "NetzKomp"}},
        {"name": "ndcfg.module_type.list", "join": {"5": "api_fkey_ndcfg_module_type_class"}},
        {"name": "nd.p_port.list", "old": {"do_connection_dest_only": True}, "join": {"3": "api_fkey_nd_p_port_mdl_loc"}},
        {"name": "nd.module.list", "join": {"6": "api_fkey_nd_module_type", "7": "api_fkey_nd_p_port_mdl_loc"}},
        {"name": "nd.l2p_port.list", "join": {"7": "api_fkey_nd_l2p_port_p"}},
        {"name": "nd.l_port.list", "join": {"9": "api_fkey_nd_l2p_port_l"}},
        {"name": "nd.vlan.list", "join": {"10": "api_fkey_nd_l_port_ingress"}}
    ])

    rooms, _, _, dd_modules, p_ports, _, _, p_ports_dest, dest_mdls, dest_l2p_ports, dest_l_ports, dest_l_ports_ingress_vlans = res

    print("\n"*5)
    print(f"Room {room} @ {bldg}")

    if len(rooms) < 1:
        print(f"Room {bldg} {room} not found in NetDB.")
        continue

    if len(dd_modules) == 0:
        if args.verbose:
            print(f"Room {bldg} {room} has no modules.")
        continue

    p_ports_by_mdl = dict(o2m(p_ports, 'mdl_fq_name'))
    p_ports_dest_by_pk = dict(o2o(p_ports_dest, 'gpk'))
    dest_mdls_by_fq_name = dict(o2o(dest_mdls, 'fq_name'))
    dest_l2p_ports_by_p_port_pk = dict(o2o(dest_l2p_ports, 'p_port_gfk'))
    dest_l_ports_by_pk = dict(o2o(dest_l_ports, 'gpk'))
    dest_l_ports_ingress_vlans_by_pk = dict(o2o(dest_l_ports_ingress_vlans, 'gpk'))

    # Iterate through all Datendosen in a Room
    for mdl in dd_modules:
        print(f"Module {mdl['fq_name']} @ {mdl['bldg']}, {mdl['room']}")

        # Iterate through all P-Ports of the module
        mdl_p_ports = p_ports_by_mdl[mdl['fq_name']]
        for p_port in mdl_p_ports:
            # Check if p_port type group is interesting to us
            # (we only want the "back" side of the ports)
            if not p_port['type_group'] in ALLOWED_P_PORT_TYPE_GROUPS:
                continue

            row = RoomDDInfo(mdl['bldg'], mdl['room'], mdl['fq_name'], p_port['name'])
            csvrows.append(row)

            print(f"* P-Port {p_port['name']} ({p_port['type_group']})")

            # Check if the p_port is connected to something
            dest_connected_p_port = p_ports_dest_by_pk.get(p_port['dest_connected_gfk'])
            if not dest_connected_p_port:
                if args.verbose:
                    print("** not patched")
                continue

            # Check if the p_ports final destination is a Netzkomponente
            dest_connection_mdl = dest_mdls_by_fq_name.get(dest_connected_p_port['mdl_fq_name'])
            if not dest_connection_mdl:
                if args.verbose:
                    print("** not connected to a network component")
                continue
            row.connected_module_name = dest_connection_mdl['fq_name']
            row.connected_p_port_name = dest_connected_p_port['name']
            
            # Get the L-Port for the destination P-Port
            l_port_mapping = dest_l2p_ports_by_p_port_pk.get(dest_connected_p_port['gpk'])

            # Check if there is a L-Port for the p_ports final destination
            if not l_port_mapping:
                if args.verbose:
                    print("** not L-Port")
                continue

            l_port = dest_l_ports_by_pk.get(l_port_mapping['l_port_gfk'])
            ingress_vlan = dest_l_ports_ingress_vlans_by_pk.get(l_port['ingress_vlan_gfk'])

            row.connected_device_fqdn = l_port['dev_fqdn']
            row.connected_l_port_name = l_port['name']

            if ingress_vlan is None:
                print(f"** Ingress VLAN not configured")
            else:
                row.vlan_id = ingress_vlan['id']
                row.vlan_netinstance = ingress_vlan['net_instnc']
                row.vlan_name = ingress_vlan['name']
                row.bcd = ingress_vlan['bcd']

                print(f"** Ingress VLAN: {row.vlan_id} @ {row.vlan_netinstance} ({row.vlan_name}, BCD: {row.bcd})")

if args.csv_out:
    with open(args.csv_out, 'w') as f:
        writer = csv.writer(f)
        for row in csvrows:
            if not filter_vlan or (filter_vlan['id'] == row.vlan_id and filter_vlan['net_instnc'] == row.vlan_netinstance):
                writer.writerow(row.to_csv())
