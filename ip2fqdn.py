#!/usr/bin/env python3

import csv
import sys

from netdb_client.api32 import APIEndpoint, APISession
import netdb_client.util
import netdb_client.dns

parser = netdb_client.util.ArgumentParser(description='print a record fqdn for ip address')
parser.add_argument('--output', '-o', required=True, help='output csv file')
parser.add_argument('file', help='file with ip addresses')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

if args.file is None:
    print(f"{args.file} not found or ambiguous.")
    sys.exit(1)

fqdns = []
with open(args.file, newline='') as csvfile:
    reader = csv.reader(csvfile, delimiter="\t")

    for row in reader:
        if len(row) < 1:
            print(f"Invalid row in file: {' '.join(row)}")
            continue

        
        ipaddr = str(row[0])
        res = netdb_client.dns.Record.list(api_session=api, data_old=ipaddr, type_old='A')
        if len(res) != 0:
            fqdn = res[0].fqdn
            res = netdb_client.dns.Fqdn.list(api_session=api, value_old=fqdn)
            description = res[0].description
        else:
            fqdn = ''
            description = ''
        fqdns.append([ipaddr, fqdn, description])

csv_writer = csv.writer(open(args.output, 'w'))
csv_writer.writerows(fqdns)
