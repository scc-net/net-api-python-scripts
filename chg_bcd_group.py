#!/usr/bin/env python3

import netdb_client
from netdb_client.api32 import APIEndpoint, APISession, nd, cntl
from netdb_client.util import ArgumentParser
import argparse
import sys

parser = ArgumentParser(description='Assigns BCD(s) to Group. If BCD have old group, removes them from it.')
parser.add_argument('-f', '--file', nargs='?', type=argparse.FileType('r'), default=sys.stdin, help='File containing BCDs (one per line)')
parser.add_argument('-d', '--delete', action='store_true', help='Delete groups with zero BCDs after BCD got moved.')
parser.add_argument('group', metavar='GROUP', help='Group Name')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)


bcds = [line.strip() for line in args.file.readlines()]

for bcd in bcds:
    search_ta = [
        nd.Bcd.list_ta(name_old=bcd),
        {'name': 'nd.bcd2group.list', 'join': {0: 'api_fkey_nd_bcd2group_bcd'}}
    ]
    search_result = api.execute_ta(search_ta)

    if search_result[1]:
        old_group = search_result[1][0]['group_name']
        nd.Bcd2group.delete(api, bcd_name_old=bcd, group_name_old=old_group)

        if args.delete and old_group.lower() != args.group.lower():
            group_search_ta = [
                cntl.Group.list_ta(name_old=old_group)
            ]
            group_result = api.execute_ta(group_search_ta)
            if group_result[0][0]['bcd_count'] == 0:
                print(f'deleting empty group "{old_group}"')
                cntl.Group.delete(api, name_old=old_group)

    nd.Bcd2group.create(api, bcd_name_new=bcd, group_name_new=args.group)
