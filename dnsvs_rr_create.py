#!/usr/bin/env python3

from netdb_client.api32 import dns, APIEndpoint, APISession;
from netdb_client import util;

parser = util.ArgumentParser(description='create resource records')
parser.add_argument('--ttl', '-l', type=int, required=False, help='set ttl for all records to value')
parser.add_argument('file', help='file with rr entries: owner fqdn, dbrt, dest fqdn - separated by spaces')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

if args.file:
    filename = args.file
    lines = [line.strip() for line in open(filename)]

ttl = None
if args.ttl:
    ttl = args.ttl

records = []
for line in lines:
    [fqdn, dbrt, data] = line.split()
    ta_element = dns.Record.create_ta(
        fqdn_new=fqdn,
        data_new=data,
        type_new=dbrt,
    )
    if ttl is not None:
        ta_element['new']['ttl'] = ttl
    records.append(ta_element)

api.execute_ta(records)
