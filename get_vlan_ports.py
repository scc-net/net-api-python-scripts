#!/usr/bin/env python3

from netdb_client.api32 import dns, nd, APIEndpoint, APISession;
from netdb_client import util;

from pprint import pprint
import operator
import sys

parser = util.ArgumentParser(description='Get device,port,bulding,room,dst_mod for a given ingress vlan.')
parser.add_argument('--netinst', help='The netinstance of the VLAN', dest='netinst', default='global_kit')
parser.add_argument('vlan', help='The VLAN id')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

if args.netinst:
    net_instnc = args.netinst

vlan = int(args.vlan)

port_data = api.execute_ta([
    # find VLAN
    {'name': 'nd.vlan.list', 'old': {'id': vlan, 'net_instnc': net_instnc}},

    # find l_ports in VLAN
    {'name': 'nd.l_port.list', 'join': {0: 'api_fkey_nd_l_port_ingress'}},

    # get p_ports from l_ports
    {'name': 'nd.l2p_port.list', 'join': {1: 'api_fkey_nd_l2p_port_l'}},
    {'name': 'nd.p_port.list', 'join': {2: 'api_fkey_nd_l2p_port_p'}, 'old': {"sorting_params_list": ['name_sortby ASC']}},

    # get connection path for p_ports
    {'name': 'nd.p_port.list', 'join': {2: 'api_fkey_nd_l2p_port_p'}, 'old': {"sorting_params_list": ['name_sortby ASC'], 'do_connection_path': True}},

    # retrieve list of module types that are sockets
    {'name': 'ndcfg.module_type_class.list', 'old': {'name': 'DD'}},
    {'name': 'ndcfg.module_type.list', 'join': {5: 'api_fkey_ndcfg_module_type_class'}}
])

vlans, l_ports, l2p, p_ports, connection_path, _, dd_module_types = port_data

if len(vlans) == 0:
    print(f"VLAN {vlan} not found in net instance {net_instnc}. If required, specify different net instance via --netinst")
    sys.exit(1)


def traverse_p_port_path(first_p_port, p_ports):
    connection = []

    p_ports_by_pk = dict(util.list_to_generator_map_one2one(p_ports, "gpk"))

    cur_p_port = first_p_port
    connection.append(cur_p_port)

    visited_modules = [cur_p_port["mdl_fq_name"]]
    while cur_p_port:
        # we already saw the module in the path -> exit
        if cur_p_port["connected_gfk"] in visited_modules:
            break

        # if module is not connected, stop here
        if not cur_p_port["is_connected"]:
            break

        # add connected module to the path
        cur_p_port = p_ports_by_pk.get(cur_p_port["connected_gfk"])
        if cur_p_port is None:
            break

        connection.append(cur_p_port)
        visited_modules.append(cur_p_port["gpk"])

        # if the connected module is internally connected, add the internally connected module to the path
        if cur_p_port["is_internal_connected"]:
            cur_p_port = p_ports_by_pk.get(cur_p_port["internal_connected_gfk"])
            if cur_p_port is None:
                break
            connection.append(cur_p_port)
            visited_modules.append(cur_p_port["gpk"])

    return connection


p2l_port = dict(util.list_to_generator_map_one2one(l2p, "p_port_gfk"))
l_ports_by_pk = dict(util.list_to_generator_map_one2one(l_ports, "gpk"))

# list of the names of all module types that are sockets
dd_module_type_names = list(map(operator.itemgetter('name'), dd_module_types))

for p_port in p_ports:
    l_port_pk = p2l_port[p_port['gpk']]['l_port_gfk']
    l_port = l_ports_by_pk[l_port_pk]

    if not p_port['name'] == l_port['name']:
        continue

    port_info = f"{l_port['dev_fqdn']},{p_port['name']},{p_port['mdl_bldg']},{p_port['mdl_room']}"

    # traverse the connection path in reverse direction until we find the first socket (which is the last in the connection path)
    path = traverse_p_port_path(p_port, connection_path)
    for port in path[::-1]:
        if port["mdl_type"] in dd_module_type_names:
            port_info += f",{port['mdl_fq_name']},{port['mdl_bldg']},{port['mdl_room']}"
            break

    print(port_info)
