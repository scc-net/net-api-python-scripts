#!/usr/bin/env python3
import csv
from argparse import FileType

import tabulate
from dns.name import from_unicode, NoParent
from netdb_client.api32 import APIEndpoint, APISession
from netdb_client.util import ArgumentParser

parser = ArgumentParser(description='Find all users who have access on specific domain.')
parser.add_argument('fqdn', type=str, help='FQDN')
parser.add_argument('--csv', type=FileType('w'), help='File to write data in CSV format')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

fqdn = from_unicode(args.fqdn)

# get FQDN and all Parent-FQDN
search_fqdns = [str(fqdn)]
while True:
    try:
        fqdn = fqdn.parent()
        search_fqdns.append(str(fqdn))
    except NoParent:
        break

ta = [
    {'idx': 'fqdn', 'name': 'dns.fqdn.list', 'old': {'value_list': search_fqdns}},
    {"idx": "group2fqdn", "name": "dns.fqdn2group.list", "join": {"fqdn": "default"}},
    {"idx": "ou2fqdn", "name": "dns.fqdn2ou.list", "join": {"fqdn": "default"}},
    {"idx": "groups", "name": "cntl.group.list", "join": {"group2fqdn": "default"}},
    {"idx": "ous", "name": "org.unit.list", "join": {"ou2fqdn": "default"}},
    {"idx": "mgr2group", "name": "cntl.mgr2group.list", "join": {"groups": "default"}},
    {"idx": "mgr2ou", "name": "cntl.mgr2ou.list", "join": {"ous": "default"}},
    {"idx": "mgr_grp", "name": "cntl.mgr.list", 'old': {'is_sub_account': False}, 'join': {'mgr2group': 'default'}},
    {"idx": "mgr_ou", "name": "cntl.mgr.list", 'old': {'is_sub_account': False}, 'join': {'mgr2ou': 'default'}},
]

result = api.execute_ta(ta, dict_mode=True)
users_unique = sorted(list({user['login_name']: user for user in result['mgr_grp'] + result['mgr_ou']}.values()), key=lambda x: x['login_name'])
user_list = [
    [f'{user["first_name"]} {user["last_name"]}', user['login_name'], user['email'], user['kit_org_unit_short_name']]
    for user in users_unique]
headers = ['Name', 'Account', 'E-Mail', 'IDM ORG']
print(f'Users having permissions to create records for (or below) "{args.fqdn}":')
print(tabulate.tabulate(user_list, headers=headers))

if args.csv is not None:
    writer = csv.writer(args.csv)
    writer.writerow(headers)
    writer.writerows(user_list)
    print(f'Wrote data into {args.csv.name}')
