#!/usr/bin/env python3

from netdb_client.api32 import dns, nd, APIEndpoint, APISession;
from netdb_client import util;

parser = util.ArgumentParser(description='ip2mgr')
parser.add_argument('cidr', help='cidr')
args = parser.parse_args()
cidr = args.cidr

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

data = api.execute_ta([
    nd.IpSubnet.list_ta(cidr_old=cidr, cidr_operator_old='smallest_cts'),
        {'name': 'nd.bcd.list', 'join': {0: 'api_fkey_nd_ip_subnet_bcd'}},
        {'name': 'nd.bcd2group.list', 'join': {1: 'api_fkey_nd_bcd2group_bcd' }},
        {'name': 'cntl.group.list', 'join': {2: 'api_fkey_nd_bcd2group_grp' }},
        {'name': 'cntl.mgr2group.list', 'join': {3: 'api_fkey_cntl_mgr2group_grp' }},
      ])

mgrs = list()
for mgr in data[4]:
    mgrs.append(mgr['mgr_login_name'])

print(mgrs)
