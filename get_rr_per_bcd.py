#!/usr/bin/env python3

import ipaddress
import fileinput

from netdb_client.api32 import APIEndpoint, APISession
import netdb_client.util
import netdb_client.dns

parser = netdb_client.util.ArgumentParser(description='get hosts per bcd')
parser.add_argument('bcd', help='bcd')
args = parser.parse_args()

bcd = args.bcd

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

rr_types = [ 'A' ]

for rr_type in rr_types:
    record_data = netdb_client.dns.Record.list(api_session=api, target_bcd_list_old=[bcd])
    for record in record_data:
        print(record.fqdn + "\t" + record.type + "\t" + record.data)
