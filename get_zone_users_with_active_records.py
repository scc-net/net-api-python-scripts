#!/usr/bin/env python3
import csv
import json
from argparse import FileType

import tabulate
from dns.name import from_unicode, NoParent
from netdb_client.api32 import APIEndpoint, APISession
from netdb_client.util import ArgumentParser

parser = ArgumentParser(description='Find all users managing records in a specific zone.')
parser.add_argument('zones', type=str, help='zones (, spereated)')
parser.add_argument('--massmail',  type=FileType('w'), help='File to write data in KIT-CERT massmailer format')
parser.add_argument('--csv', type=FileType('w'), help='File to write data in CSV format')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

users_unique = {}
for z in args.zones.split(','):
    zone = from_unicode(z)

    ta = [
        {'idx': 'fqdn', 'name': 'dns.fqdn.list', 'old': {'zone': str(zone), 'has_rr': True}},
        {"idx": "records", 'name': 'dns.record.list', 'join': {'fqdn': 'default'}}
    ]
    result = api.execute_ta(ta)
    name_list = []
    for r in result[1]:
        if r['target_bcd_list'] is None:
            continue
        name_list += r['target_bcd_list']
    name_list = list({t: t for t in name_list}.values()) 
    for n in name_list:
        ta = [
            {"idx": "bcds", 'name': 'nd.bcd.list', 'old': {'name': n}},
            {"idx": "group2bcd", "name": "nd.bcd2group.list", "join": {"bcds": "default"}},
            {"idx": "ou2bcd", "name": "nd.bcd2ou.list", "join": {"bcds": "default"}},
            {"idx": "groups", "name": "cntl.group.list", "join": {"group2bcd": "default"}},
            {"idx": "ous", "name": "org.unit.list", "join": {"ou2bcd": "default"}},
            {"idx": "mgr2group", "name": "cntl.mgr2group.list", "join": {"groups": "default"}},
            {"idx": "mgr2ou", "name": "cntl.mgr2ou.list", "join": {"ous": "default"}},
            {"idx": "mgr_grp", "name": "cntl.mgr.list", 'old': {'is_sub_account': False}, 'join': {'mgr2group': 'default'}},
            {"idx": "mgr_ou", "name": "cntl.mgr.list", 'old': {'is_sub_account': False}, 'join': {'mgr2ou': 'default'}},
        ]

        result = api.execute_ta(ta)
        users = result[7] + result[8]
        for u in users:
            if u['login_name'] not in users_unique:
                u['bcds'] = {}
                users_unique[u['login_name']] = u
            users_unique[u['login_name']]['bcds'][n] = n

headers = ['Name', 'Account', 'E-Mail', 'IDM ORG', 'BCDs']
user_list = [
    [f'{user["first_name"]} {user["last_name"]}', user['login_name'], user['email'], user['kit_org_unit_short_name'], ','.join(list(user['bcds'].values()))]
    for user in users_unique.values()]
print(f'Users managing records unter zones "{args.zones}":')
print(tabulate.tabulate(user_list, headers=headers))

if args.massmail is not None:
    cont = dict()
    cont['globals'] = {
        'additional_headers': {
            'Reply-To': 'netvs@scc.kit.edu',
            'X-Auto-Response-Suppress': 'All',
            'Precedence': 'Bulk',
            'Auto-Submitted': 'yes'
        }
    }
    cont['individual'] = list()
    for u in users_unique.values():
        cont['individual'].append({
            'to': u['email'],
            'name': f'{u["first_name"]} {u["last_name"]}',
            'login_name': u['login_name'],
            'bcds': list(u['bcds'].values())
            })
    args.massmail.write(json.dumps(cont, indent=4))
    print(f'Wrote massmail data into {args.massmail.name}')

if args.csv is not None:
    writer = csv.writer(args.csv)
    writer.writerow(headers)
    writer.writerows(user_list)
    print(f'Wrote data into {args.csv.name}')
