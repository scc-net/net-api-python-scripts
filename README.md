# Disclaimer

Diese Einzelskripte sind nur als Beispiele gedacht. Das gehört alles generalisiert.

# Library

Hier findet man die verwendete Library für die Nutzung der NETDB-API:
https://gitlab.kit.edu/scc-net/netvs/netdb-client

Einfacher ist es jedoch `poetry` zur Bibliotheksverwaltung und Ausführen der Skripte zu nutzen.

Vor der Ausführung mit `poetry run` mus einmal `poetry update` ausgeführt werden.
Um die netdb-client Bibliothek zu aktualisieren, kann der selbe Befehl wieder ausgeführt werden.

# Example use of `get_users_for_ips.py` with a new line seperated file of addresses or cidrs
`poetry run python3 get_users_for_ips.py --massmail data.json $(cat addresses.txt | sed ':a;N;$!ba;s/\n/,/g' | sed 's/,$//')`
