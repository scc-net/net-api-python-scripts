#!/usr/bin/env python3

from netdb_client.api32 import APIEndpoint, APISession
import netdb_client.util
import netdb_client.dns

parser = netdb_client.util.ArgumentParser(description='get fqdn descriptions for bcd')
parser.add_argument('bcd', help='bcd')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

bcd = args.bcd
record_data = netdb_client.dns.Record.list(api_session=api, target_bcd_list_old=[bcd], type_old='A')

for record in record_data:
    f = record.fqdn
    d = ''
    if record.fqdn_description is not None:
        d = record.fqdn_description
        print('{}: {}'.format(f, d))
