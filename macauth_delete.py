#!/usr/bin/env python3

import csv
import sys

from netdb_client.api32 import macauth, nd, APIEndpoint, APISession;
from netdb_client import util;

parser = util.ArgumentParser(description='delete macauth entries')
parser.add_argument('file', help='file with list of mac addresses')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
client = APISession(endpoint)

ta = []
with open(args.file, newline='') as csvfile:
  reader = csv.reader(csvfile, delimiter="\t")

  for row in reader:
  	if len(row) < 1:
  		print(f"Invalid row in file: {' '.join(row)}")
  		continue

  	mac = row[0]
  	ta.append(macauth.Client.delete_ta(mac_addr_old=mac))

client.execute_ta(ta)
