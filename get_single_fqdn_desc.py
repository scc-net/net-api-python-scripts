#!/usr/bin/env python3

from netdb_client.api32 import APIEndpoint, APISession
import netdb_client.util
import netdb_client.dns

parser = netdb_client.util.ArgumentParser(description='get fqdn description for single fqdn')
parser.add_argument('fqdn', help='fqdn')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

fqdn_result = netdb_client.dns.Fqdn.list(api, value_old=args.fqdn)

if fqdn_result:
    print('FQDN: {}'.format(fqdn_result[0].value))
    print('Description: {}'.format(fqdn_result[0].description))
else:
    print('No FQDN "{}" found'.format(args.fqdn))
