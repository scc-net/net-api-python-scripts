#!/usr/bin/env python3
import csv
import json
from argparse import FileType

import tabulate
from netdb_client.api41 import APIEndpoint, APISession
from netdb_client.util import ArgumentParser, list_to_generator_map_one2many, list_to_generator_map_one2one

parser = ArgumentParser(description='Get all devices where a BCD is used with their respective VLAN IDs.')
parser.add_argument('--csv', type=FileType('w'), help='File to write data in CSV format')
parser.add_argument('--dev_type', type=str, help='Filter devices by type')
parser.add_argument('bcds', type=str, help='BCDs (, spereated)')
args = parser.parse_args()

bcds = [b.strip() for b in args.bcds.split(',')]
endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)
ta = [
    {"idx": "bcds", "name": "nd.bcd.list", "old": {"name_list": bcds}},
    {"idx": "vlan", "name": "nd.vlan.list", "inner_join_ref": {'bcds': 'default'}},
    {"idx": "v2d", "name": "nd.vlan2device.list", "inner_join_ref": {'vlan': 'default'}},
]
if args.dev_type is None:
    ta.append({"idx": "dev", "name": "nd.device.list", "inner_join_ref": {'v2d': 'default'}})
else:
    ta.append({"idx": "dev", "name": "nd.device.list", "inner_join_ref": {'v2d': 'default'}, "old": {"type": args.dev_type}})
result = api.execute_ta(ta)
vlans_by_gpk = dict(list_to_generator_map_one2one(result[1], 'gpk'))
bcds_by_name = dict(list_to_generator_map_one2one(result[0], 'name'))
dev_by_fqdn = dict(list_to_generator_map_one2one(result[3], 'fqdn'))
headers = ['Device', 'BCD', 'VLAN ID', 'Netinstance', 'Device Type']
result_list = [[v2d['dev_fqdn'], bcds_by_name[vlans_by_gpk[v2d['vlan_gfk']]['bcd']]['name'], vlans_by_gpk[v2d['vlan_gfk']]['id'], vlans_by_gpk[v2d['vlan_gfk']]['net_instnc'], dev_by_fqdn[v2d['dev_fqdn']]['type']]
    for v2d in result[2] if v2d['dev_fqdn'] in dev_by_fqdn]
print(f'Devices and repsective VLANs for BCDs "{args.bcds}":')
print(tabulate.tabulate(result_list, headers=headers))


if args.csv is not None:
    writer = csv.writer(args.csv)
    writer.writerow(headers)
    writer.writerows(result_list)
    print(f'Wrote CSV data into {args.csv.name}')
