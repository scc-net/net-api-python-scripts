#!/usr/bin/env python3

from netdb_client.api32 import dns, APIEndpoint, APISession;
from netdb_client import util;

parser = util.ArgumentParser(description='create fqdns')
parser.add_argument('file', help='file with fqdn entries: fqdn, dbnt - separated by spaces')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

if args.file:
    filename = args.file
    lines = [line.strip() for line in open(filename)]

fqdns = []
for line in lines:
    if line.strip():
        [fqdn, dbnt] = line.split()
        fqdns.append(
            dns.Fqdn.create_ta(
                value_new=fqdn,
                type_new=dbnt,
            )   
        )

api.execute_ta(fqdns)
