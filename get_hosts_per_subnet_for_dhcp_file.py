#!/usr/bin/env python3

import ipaddress
import fileinput

from netdb_client.api32 import APIEndpoint, APISession
import netdb_client.dns
import netdb_client.util

parser = netdb_client.util.ArgumentParser(description='get hosts per ipv4 or ipv6 subnet')
parser.add_argument('filename', help='file with ip and mac addresses')
parser.add_argument('bcd', help='bcd')

args = parser.parse_args()

filename = args.filename
bcd = args.bcd

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

dbrt="A"
record_data = netdb_client.dns.Record.list(api_session=api, target_bcd_list_old=[bcd], type_old=dbrt)

fqdn = dict()
for record in record_data:
    fqdn[record.data] = record.fqdn

m_dhcp_ipaddrs = dict()
for line in fileinput.input(args.filename):
    l = line.rstrip()
    [ipaddr, macaddr] = l.split()
    m_dhcp_ipaddrs[ipaddr] = macaddr

ipaddrs = m_dhcp_ipaddrs.keys()
sorted_list_of_ipaddrs = map(str,sorted(map(ipaddress.ip_address, ipaddrs)))

for ipaddr in sorted_list_of_ipaddrs:
    f = fqdn[ipaddr]
    f = f.split('.')[0]
    print('\t'.join([ipaddr, f, m_dhcp_ipaddrs[ipaddr]]))
