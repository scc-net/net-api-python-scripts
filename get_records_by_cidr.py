#!/usr/bin/env python3

from netdb_client.api41 import APIEndpoint, APISession
from netdb_client import util

parser = util.ArgumentParser(description='Get BCD by cidr')
parser.add_argument('cidr', help='The cidr')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

cidr = args.cidr

ta_data = api.execute_ta([
    {"idx": "ips", "name": "dns.ip_addr.list", "old": {"value_cidr": cidr}},
    {"idx": "records", "name": "dns.record.list", "inner_join_ref": {"ips": "default"}},
])

_, records = ta_data

print("\n".join([str(r) for r in records]))
