#!/usr/bin/env python3

import pprint

from netdb_client.api32 import APIEndpoint, APISession, dns
from netdb_client.util import ArgumentParser

parser = ArgumentParser(description='Find all records for a specific ou.')
parser.add_argument('ou', type=str, help='ou')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

ou = args.ou
zones = [ "fzk.de", "uni-karlsruhe.de", "uka.de" ]

for zone in zones:
   ta = [
       {'idx': 'fqdn', 'name': 'dns.fqdn.list', 'old': {'zone': str(zone), 'has_rr': True}},
       {"idx": "records", 'name': 'dns.record.list', 'join': {'fqdn': 'default'}},
       {"idx": "ous", "name": "org.unit.list", 'old': {'short_name': ou, 'do_subtree': True}},
       {"idx": "ou2bcd", "name": "nd.bcd2ou.list", "join": {"ous": "default"}},
       {"idx": "bcds", 'name': 'nd.bcd.list', "join": {"ou2bcd": "default"}},
   ]
   res = api.execute_ta(ta)
   bcds = [bcd['name'] for bcd in res[4]]
   records = [record for record in res[1] if record['target_bcd_list'] is not None and bool(set(record['target_bcd_list']) & set(bcds))]
   for r in records:
       print("\t".join([r['fqdn'], r['type'], r['data'], r['target_bcd_list'][0]]))
