#!/usr/bin/env python3
import ipaddress

from netdb_client.api41 import APIEndpoint, APISession
from netdb_client.util import ArgumentParser

parser = ArgumentParser(
    description='create gateway resource records for a BCD. '
                '(This script is only built for anycast-gateways, and BCDs with a single VLAN)'
)
parser.add_argument('bcd', type=str, help='BCD to create gateway records for')
parser.add_argument('--domain', '-d', type=str, default='scc.kit.edu', help='domain for the GW-Record')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

domain = args.domain
gateway_fqdn_tmpl = 'defgw-v{vni}.{domain}'

search_transaction = [
    {'idx': 'bcd', 'name': 'nd.bcd.list', 'old': {'name': args.bcd}},
    {'idx': 'vxlan', 'name': 'nd.vxlan.list', 'inner_join_ref': {'bcd': 'default'}},
    {'idx': 'subnet', 'name': 'nd.ip_subnet.list', 'inner_join_ref': {'bcd': 'default'}},
]
search_result = api.execute_ta(search_transaction, dict_mode=True)

assert len(search_result['bcd']) == 1, f'Found {len(search_result[0])} BCDs, expected 1.'
assert len(search_result['vxlan']) == 1, f'Found {len(search_result[1])} VXLANs, expected 1.'
assert len(search_result['subnet']) > 0, 'Found 0 Subnets, expected at least 1.'
assert len(search_result['subnet']) <= 2, f'Found {len(search_result[2])} Subnets, expected a maximum of 2.'

found = {'4': False, '6': False}
for subnet in search_result['subnet']:
    if found[subnet['type']]:
        raise ValueError(f'Found multiple Subnets of same IP-Version (v{subnet["type"]})')
    else:
        found[subnet['type']] = True

gateways = [ipaddress.ip_interface(subnet['default_gateway']) for subnet in search_result['subnet']]
gateway_fqdn = gateway_fqdn_tmpl.format(vni=search_result['vxlan'][0]['vni'], domain=domain)

wanted_records = []
for gateway in gateways:
    if gateway.version == 6:
        rr_type = 'AAAA'
    elif gateway.version == 4:
        rr_type = 'A'
    else:
        raise ValueError
    wanted_records.append({'type': rr_type, 'data_list': [str(gateway.ip)]})

create_transaction = [
    {'idx': 'list_fqdn', 'name': 'dns.fqdn.list', 'old': {'value_list': [gateway_fqdn]}},
    {'idx': 'create_fqdn', 'name': 'dns.fqdn.create', 'new': {'value': gateway_fqdn, 'type': 'domain'}, 'when': {'returns_no_data': ['list_fqdn']}},
    {'idx': 'import_records', 'name': 'dns.record.imp', 'new': {'fqdn': gateway_fqdn, 'import_data': {'dns.record_imp': wanted_records}}}
]
create_result = api.execute_ta(create_transaction)

print('Success!')
print(f'Created RRs from {gateway_fqdn} to:')
for gateway in gateways:
    print(gateway.ip)
