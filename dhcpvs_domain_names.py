import ipaddress
import json
import pprint

from netdb_client.api32 import APIEndpoint, APISession, nd, cntl
from netdb_client.util import ArgumentParser, list_to_generator_map_one2many

parser = ArgumentParser(description='Script to set the DHCP Option "domain-name" (option 15) for a subnet.')
parser.add_argument('fqdn', metavar='FQDN', help='FQDN to set as domain-name for subnets')
parser.add_argument('subnet', metavar='SUBNET', nargs='+', help='Subnets in CIDR Notation')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

fqdn = args.fqdn

for subnet_arg in args.subnet:
    subnet = ipaddress.ip_network(subnet_arg)

    ta = [
        {'idx': 'subnet', 'name': 'nd.ip_subnet.list', 'old': {'cidr': str(subnet)}},
        {'idx': 'option', 'name': 'dhcp.subnet_option.create', 'new': {'code': 15, 'data': fqdn}, 'new_ref': {'ip_subnet_cidr': {'idx': 'subnet', 'param': 'cidr'}}}
    ]

    api_result = api.execute_ta(ta, dict_mode=True)
    print(f'Added "{fqdn}" to subnet "{str(subnet)}".')
