#!/usr/bin/env python3
import netdb_client
from netdb_client.api32 import APIEndpoint, APISession, org
from netdb_client.util import ArgumentParser, list_to_generator_map_one2one
import sys

parser = ArgumentParser(description='Find all BCDs of an ORG')
parser.add_argument('org', metavar='ORG', help='Organization Unit Shortname', type=str)
parser.add_argument('--orphans', help='Only Search for BCDs without groups', action='store_true')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

ta = [
    {'idx': 'org', 'name': 'org.unit.list', 'old': {'short_name': args.org}},
    {'idx': 'o2b', 'name': 'nd.bcd2ou.list', 'join': {'org': 'default'}},
    {'idx': 'bcd', 'name': 'nd.bcd.list', 'join': {'o2b': 'default'}}
]
if args.orphans:
    ta.append( {'name': 'nd.bcd2group.list', 'join': {'bcd': 'api_fkey_nd_bcd2group_bcd'}})
bcds = api.execute_ta(ta)

if not bcds[0]:
    print(f'Did not find ORG "{args.org}"')
    sys.exit(2)

if not bcds[1]:
    print(f'ORG "{args.org}" has no BCDs.')
    sys.exit(0)

if len(bcds[1]) == 1:
    print(f'Found BCD for ORG "{args.org}": {bcds[1][0]["name"]}')
    sys.exit(0)

if args.orphans:
    bcd2group = [key for key, val in list_to_generator_map_one2one(bcds[2], 'bcd_name')]
    bcd_list = [item['name'] for item in bcds[2] if item['name'] not in bcd2group]
else:
    bcd_list = [item['name'] for item in bcds[2]]

print(f'Found BCDs for ORG "{args.org}": {", ".join(bcd_list)}')
