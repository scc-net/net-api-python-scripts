#!/usr/bin/env python3

from netdb_client.api32 import dns, APIEndpoint, APISession;
from netdb_client import util;
import re

parser = util.ArgumentParser(description='find fqdn by its description. ATTENTION: this performs poorly with global read rights.')
parser.add_argument('--bcd', help='Limit search to BCD')
parser.add_argument('description', help='description')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

if args.bcd:
    fqdn_result = dns.Fqdn.list(api, rr_chain_target_bcd_list_old=[args.bcd])
else:
    print('ATTENTION: If your api token has global read rights this could take some time.')
    fqdn_result = dns.Fqdn.list(api)
descr_filter = re.compile(re.escape(args.description), re.IGNORECASE)

filtered_result = [fqdn for fqdn in fqdn_result if descr_filter.search(str(fqdn.description))]

if filtered_result:
    for fqdn in filtered_result:
        print('FQDN: {}'.format(fqdn.value))
        print('Description: {}'.format(fqdn.description))
else:
    print('No FQDN with description "{}" found.'.format(args.description))
