#!/usr/bin/env python3
from netdb_client.api32 import APIEndpoint, APISession, nd, cntl, dns
from netdb_client.util import ArgumentParser
import sys

parser = ArgumentParser(description='BCD Migration Helper. Moves BCD to new OU including the group which contains the bcd.')
parser.add_argument('bcd', metavar='BCD', help='BCD name', type=str)
parser.add_argument('org', metavar='ORG', help='Organization Unit Shortname', type=str)
parser.add_argument('--force', help='Force BCD and Group movement if Group has multiple BCDs.', action='store_true')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

has_groups = False
group_names = None
bcds_in_groups = {}
groups_have_multiple_bcds = False
group_fqdns_before_move = {}

search_ta = [
    {'idx': 'bcd', 'name': 'nd.bcd.list', 'old': {'name': args.bcd}},  # Get BCD
    {'idx': 'bcd2ou', 'name': 'nd.bcd2ou.list', 'join': {'bcd': 'default'}},  # Get BCD Orgs
    {'idx': 'bcd2group', 'name': 'nd.bcd2group.list', 'join': {'bcd': 'default'}},  # Get Group2BCD Mapping
    {'idx': 'group', 'name': 'cntl.group.list', 'join': {'bcd2group': 'default'}},  # Get Groups
    {'idx': 'group2bcd', 'name': 'nd.bcd2group.list', 'join': {'group': 'default'}},  # Check for more BCDs in Groups
    {'idx': 'group2fqdn', 'name': 'dns.fqdn2group.list', 'join': {'group': 'default'}},  # Get Groups FQDNs
    {'idx': 'org', 'name': 'org.unit.list', 'old': {'short_name': args.org}}  # Check ORG
]
search = api.execute_ta(search_ta, dict_mode=True)

subgroups = [group['name'] for group in search['group'] if group['is_sub_group']]

# Abort if BCD has more than 1 ORG
if len(search['bcd2ou']) != 1:
    print(f'Found multiple assigned Orgs for BCD {args.bcd}: {",".join([item["ou_short_name"] for item in search["bcd2ou"]])}')
old_org = search['bcd2ou'][0]['ou_short_name']
# Abort if BCD not exists
if len(search['bcd']) == 0:
    print(f'No BCD "{args.bcd}" found.')
    sys.exit(2)
# Abort if ORG not exists
if len(search['org']) != 1:
    print(f'No ORG with shortname "{args.org}" found.')
    sys.exit(2)
# Abort if BCD is already in ORG
if old_org == args.org:
    print(f'BCD "{args.bcd}" is allready in ORG "{args.org}".')
    sys.exit(2)
# Check if BCD has Groups
if len(search['bcd2group']) != 0:
    has_groups = True
    group_names = [group['name'] for group in search['group']]
# Check if Groups has multiple BCDs
if has_groups and any(group['bcd_count'] != 1 for group in search['group']):
    groups_have_multiple_bcds = True
    for g2b in search['group2bcd']:
        group = g2b['group_name']
        if group in bcds_in_groups:
            bcds_in_groups[group].append(g2b['bcd_name'])
        else:
            bcds_in_groups[group] = [g2b['bcd_name']]

    print('Groups have multiple BCDs:')
    for group, bcds in bcds_in_groups.items():
        print(f'  {group}: {", ".join(bcds)}')
    if not args.force:
        print('Exiting...')
        sys.exit(2)
    print('Forced movement anyway. Continuing...')

if len(search['group2fqdn']) != 0:
    for g2f in search['group2fqdn']:
        group = g2f['group_name']
        if group in group_fqdns_before_move:
            group_fqdns_before_move[group].append(g2f['fqdn_value'])
        else:
            group_fqdns_before_move[group] = [g2f['fqdn_value']]

move_bcd_ta = [
    nd.Bcd2ou.delete_ta(bcd_name_old=args.bcd, ou_short_name_old=old_org),
    nd.Bcd2ou.create_ta(bcd_name_new=args.bcd, ou_short_name_new=args.org)
]
move_bcd = api.execute_ta(move_bcd_ta)
print(f'Moved "{args.bcd}" to "{args.org}".')

if not has_groups:
    print(f'"{args.bcd}" has no group, finished moving to "{args.org}"')
else:
    maingroups_first_then_subgroups = [group for group in group_names if group not in subgroups] + subgroups
    for group in maingroups_first_then_subgroups:
        if group in subgroups:
            move_group_ta = [
                nd.Bcd2group.create_ta(bcd_name_new=args.bcd, group_name_new=group)
            ]
            move_group = api.execute_ta(move_group_ta)
            print(f'Readded BCD "{move_group[0][0]["bcd_name"]}" to group "{group}".')
        else:
            move_group_ta = [
                cntl.Group.update_ta(name_old=group, ou_short_name_new=args.org),
                nd.Bcd2group.create_ta(bcd_name_new=args.bcd, group_name_new=group),
            ]
            move_group = api.execute_ta(move_group_ta)
            print(f'Moved Group "{move_group[0][0]["name"]}" to ORG {move_group[0][0]["ou_short_name"]} and readded BCD "{move_group[1][0]["bcd_name"]}".')

        fqdns_before_move = group_fqdns_before_move[group] if group in group_fqdns_before_move else []
        groupfqdns_after_move = dns.Fqdn2group.list(api, group_name_old=group)
        if len(groupfqdns_after_move) != 0:
            fqdns_after_move = [item.fqdn_value for item in groupfqdns_after_move]
            lost_fqdns = [item for item in fqdns_before_move if item not in fqdns_after_move]
        else:
            lost_fqdns = fqdns_before_move
        if lost_fqdns:
            print(f'FQDNs which were lost from Group: {", ".join(lost_fqdns)}')
        if groups_have_multiple_bcds:
            print(
                f'Orphaned BCDs in "{old_org}": {", ".join([bcd for bcd in bcds_in_groups[group] if bcd != args.bcd])}')

