#!/usr/bin/env python3
import ipaddress
from pprint import pprint
import re
import sys

import netdb_client
import netdb_client.util
import netdb_client.dns

parser = netdb_client.util.ArgumentParser(description='allocate KIT-Core loopbacks')
parser.add_argument('vrf', help='name of VRF')
parser.add_argument('module_name', help='hostname of first gridka device, not fqdn, e.g. rcs-2021-l-13-1')
args = parser.parse_args()

endpoint = netdb_client.APIEndpoint(**vars(args))
api = netdb_client.APISession(endpoint)

# Find max allocated BCD number
BCD_NAME_REGEXP = "^gridka-lo10-([0-9]{1,4})$"
loopback_bcds, mdl = api.execute_ta([
	{"name": "nd.bcd.list", "old": {
		"name_regexp": BCD_NAME_REGEXP
	}},
	{"name": "nd.module.list", "old": {
		"fq_name": args.module_name
	}}
])

max_allocated_number = -1
for existing_bcd in loopback_bcds:
	number = int(re.search(BCD_NAME_REGEXP, existing_bcd['name']).group(1))
	max_allocated_number = max(max_allocated_number, number)

if args.vrf != 'gridka':
    print(f'Invalid/unknown VRF {args.vrf}.')
    sys.exit(1)

# Check if we can find modules with the specified names
if len(mdl) != 1:
	print(f'No NETDOC Module found for leaf {args.module_name}')
	sys.exit(1)

loopback_interface_number = 10

ipv4_base_address = ipaddress.ip_address('10.127.0.0')
ipv6_base_address = ipaddress.ip_address('2a00:139c:f::')

loopback_bcd_1_number = max_allocated_number + 1
bcd_1_name = f'gridka-lo10-{loopback_bcd_1_number}'
bcd_1_description = f'loopback{loopback_interface_number} for VRF {args.vrf} on {args.module_name}'
bcd_1_ipv4 = str(ipv4_base_address + loopback_bcd_1_number)
bcd_1_ipv6 = str(ipv6_base_address + loopback_bcd_1_number)
bcd_1_hostname = f'{args.module_name}-lo{loopback_interface_number}.net.gridka.de'

print(f'Maximum allocated loopback BCD number is {max_allocated_number}, using {loopback_bcd_1_number}')
print(f'Creating BCD {bcd_1_name}')
print(f'* {bcd_1_description}')
print(f'* IPv4: {bcd_1_ipv4}/32, IPv6: {bcd_1_ipv6}/128')
print(f'* Hostname: {bcd_1_hostname}')
print()
print()

print('Router config:')
print(f'! {args.module_name}')
print(f'interface loopback{loopback_interface_number}')
print(f'  description vrf-{args.vrf}')
print(f'  vrf member {args.vrf}')
print(f'  ip address {bcd_1_ipv4}/32')
print(f'  ipv6 address {bcd_1_ipv6}/128')
print()

print()
print()
print('Continue? [yn]')
res = input()
if res != 'y':
	print('Exiting.')
	sys.exit()


ta = []
ta.append({
    "name": "nd.bcd.create",
    "new": {
        "name": bcd_1_name,
        "categ": "ROUTER-LOOPBACK",
        "seclvl": 210,
        "description": bcd_1_description
    }
})
ta.append({
    "name": "nd.bcd2ou.create",
    "new": {
        "bcd_name": bcd_1_name,
        "ou_short_name": "SCC-NET",
    }
})
ta.append({
    "name": "nd.bcd2group.create",
    "new": {
        "bcd_name": bcd_1_name,
        "group_name":  "scc-net-routing"
    }
})
ta.append({
    "name": "nd.ip_subnet.create",
    "new": {
        "bcd": bcd_1_name,
        "cidr": f'{bcd_1_ipv4}/32',
        "create_ipaddrs": False,
        "auto_create_ip_address": True
    }
})
ta.append({
    "name": "nd.ip_subnet.create",
    "new": {
        "bcd": bcd_1_name,
        "cidr": f'{bcd_1_ipv6}/128',
        "create_ipaddrs": False,
        "auto_create_ip_address": True
    }
})
ta.append({
    "name": "dns.record.create",
    "new": {
        "fqdn": bcd_1_hostname,
        "data": bcd_1_ipv4,
        "type": "A"
    }
})
ta.append({
    "name": "dns.record.create",
    "new": {
        "fqdn": bcd_1_hostname,
        "data": bcd_1_ipv6,
        "type": "AAAA"
    }
})


api.execute_ta(ta)
