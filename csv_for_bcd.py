#!/usr/bin/env python3

from netdb_client.api32 import APIEndpoint, APISession
import netdb_client.util
import netdb_client.dns

import csv

parser = netdb_client.util.ArgumentParser(description='get data by bcd')
parser.add_argument('--output', '-o', required=True, help='csv file')
parser.add_argument('bcd', help='bcd')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
api = APISession(endpoint)

bcd = args.bcd
header = ['fqdn', 'fqdn_description', 'type', 'data']

record_data = netdb_client.dns.Record.list(api, target_bcd_list_old=[bcd])
filtered_records = [[getattr(record, h) for h in header] for record in record_data if record.type in ['A', 'AAAA']]

data = [header, *filtered_records]

csv_writer = csv.writer(open(args.output, 'w'), delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
csv_writer.writerows(data)
