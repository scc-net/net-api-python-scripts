#!/usr/bin/env python3

import itertools
import os
import sys
import termios

import json
from netdb_client.api32 import APIEndpoint, APISession
from netdb_client.api32.cntl import OtAttrVal
from netdb_client.api32.evlog import Record
from netdb_client import util


def yesno(question, choices=((('y','yes','ja','j','oui'),True),(('n', 'no','n','nein'),False)), default_choice=None, retries=None):
    prompt_choices = [c[0][0] for c in choices]
    if default_choice is not None:
        prompt_choices[default_choice] = prompt_choices[default_choice].upper()
    prompt = "{} [{}] ".format(question, "/".join(prompt_choices))
    if retries is None:
        retry_condition = itertools.count(1)
    else:
        retry_condition = range(0, retries)
    for _ in  retry_condition:
        termios.tcflush(sys.stdin, termios.TCIFLUSH);
        choice = input(prompt).lower()
        if default_choice is not None and choice == '':
            return choices[default_choice][1]
        for c in choices:
            if choice in c[0]:
                return c[1]
        else:
            print('invalid selection!')
    else:
        raise NotImplementedError


parser = util.ArgumentParser(description='update VRF')
parser.add_argument('old_vrf', help='old_vrf')
parser.add_argument('new_vrf', help='old_vrf')
parser.add_argument('-y', action="store_true", default=False, help='Do not ask')
args = parser.parse_args()

endpoint = APIEndpoint(**vars(args))
session = APISession(endpoint)

ta_list = [
    {
        "idx": "bcd",
        "name": "nd.bcd.list",
        "old": {},
    },
    {
        "name": "cntl.ot_attr_val.list",
        "old": {
            "ot_attr_def_key_word": "svi_config",
        },
        "join": {"bcd": None},
    },
]

bcds, svi_config_ot_attrs = session.execute_ta(ta_list)

assert len(bcds) > 0
assert len(bcds) == len(svi_config_ot_attrs)

change_bcds = []
ta_update = []
for bcd, svi_config_ot_attr in zip(bcds, svi_config_ot_attrs):
    svi_config = svi_config_ot_attr['value']
    if svi_config is not None and svi_config['vrf'] == args.old_vrf:
        change_bcds.append(bcd['name'])
        svi_config['vrf'] = args.new_vrf
        ta_update.append(OtAttrVal.bulk_update_ta(
            key_val_dict_new={"svi_config": svi_config},
            object_gfk_old=svi_config_ot_attr["object_gfk"],
        ))
        ta_update.append(Record.create_ta(
            data_new=f"SVI Config aktualisiert.\nGrund: VRF",
            object_gfk_new=svi_config_ot_attr["object_gfk"],
        ))

print('Update on the following BCDs is needed:')
print(os.linesep.join(change_bcds))
print()
if not yesno("Updates BCDs?", default_choice=1):
    sys.exit(1)

session.execute_ta(ta_update)